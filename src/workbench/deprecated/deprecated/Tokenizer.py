import copy
import typing

from deprecation import deprecated

from data.dataset.Event import Event
from data.dataset.NoneEvent import NoneEvent
from data.dataset.Trace import Trace


@deprecated
class Tokenizer:

    def __init__(self,
                 token_size: int = 2,
                 start_index: int = 0,
                 fill_nan: bool = True):
        self._token_size = token_size
        self._start_index = start_index
        self._fill_nan = fill_nan

    def tokenize(self, trace: 'Trace') -> typing.List['Trace']:
        temp_trace = copy.deepcopy(trace)
        trace_tokens = []
        if self._token_size > len(temp_trace):
            if self._fill_nan:
                nan_events: typing.List['Event'] = [NoneEvent() for _ in range(self._token_size - len(trace))]
                temp_trace.events = nan_events + temp_trace.events
            else:
                raise ArithmeticError('Token size exceeds trace length!')

        start_index = len(temp_trace) - self._token_size + self._start_index + 1 \
            if self._start_index < 0 \
            else self._start_index
        for i in range(start_index, len(temp_trace) - self._token_size + 1):
            trace_tokens.append(temp_trace[i:(i + self._token_size)])
        return trace_tokens
