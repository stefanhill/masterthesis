import typing

from data.dataset.Event import Event
from data.dataset.Trace import Trace
from ml.AbstractEncoder import AbstractEncoder


class NGramEncoder(AbstractEncoder):

    def __init__(self,
                 event_features: typing.List[str] = None,
                 trace_features: typing.List[str] = None,
                 target: str = None,
                 delimiter: str = '$'):
        self._event_features = event_features if event_features is not None else []
        self._trace_features = trace_features if trace_features is not None else []
        self._target = target
        self._delimiter = delimiter

    def encode(self, trace: 'Trace', events: typing.List['Event'], include_target=True) -> typing.Tuple[str, str]:
        prefix = self._delimiter
        label = None
        if include_target:
            last_event = events[-1]
            if self._target in getattr(last_event, '_attributes').keys():
                label = str(getattr(last_event, '_attributes')[self._target])
            elif self._target in getattr(trace, '_attributes').keys():
                label = str(getattr(trace, '_attributes')[self._target])
            else:
                raise KeyError('Target label does not exist!')
        for i in range(len(events) - include_target):
            for feature in self._event_features:
                if feature in getattr(events[i], '_attributes').keys():
                    prefix = prefix + str(getattr(events[i], '_attributes')[feature]) + self._delimiter
        for feature in self._trace_features:
            if feature in getattr(trace, '_attributes').keys():
                prefix = prefix + str(getattr(trace, '_attributes')[feature]) + self._delimiter
        return prefix, label
