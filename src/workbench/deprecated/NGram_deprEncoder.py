import typing

from data.dataset.EndEvent import EndEvent
from data.handler.EventLogHandler import EventLogHandler
from ml import AbstractPrediction
from ml.AbstractClassifier import AbstractClassifier
from ml.AbstractMetric import AbstractMetric
from utils.Decorators import timer, memory
from utils.Path import Path
from utils.Utils import Utils
from workbench.deprecated.deprecated.NGramEncoder import NGramEncoder


class NGram_deprEncoder(AbstractClassifier):

    def __init__(self, n: int = 2, **kwargs):
        super().__init__(**kwargs)
        self._n = n
        self._gram_to_probability: typing.Dict[int, typing.Dict[str, typing.Dict[str, float]]] = {}

    @property
    def encoder(self) -> 'NGramEncoder':
        return self._encoder

    @encoder.setter
    def encoder(self, value: 'NGramEncoder'):
        self._encoder = value

    @timer
    @memory
    def fit(self, data: 'EventLogHandler', evaluation_data: 'EventLogHandler' = None):

        self._gram_to_probability = {}

        for n in range(1, self._n + 1):

            self._gram_to_probability[n] = {}
            for trace in data.dataset.traces:

                i = 0
                while i < len(trace) - n:
                    prefix = Utils.list_to_string([event.identifier for event in trace.events[i:i + n]])
                    if prefix not in self._gram_to_probability[n].keys():
                        self._gram_to_probability[n][prefix] = {}
                    label = trace.events[i + n].identifier
                    if label not in self._gram_to_probability[n][prefix].keys():
                        self._gram_to_probability[n][prefix][label] = 0
                    self._gram_to_probability[n][prefix][label] += 1

                    i += 1

            for prefix in self._gram_to_probability[n].keys():
                total = sum(self._gram_to_probability[n][prefix].values())
                if total > 0:
                    for label in self._gram_to_probability[n][prefix].keys():
                        self._gram_to_probability[n][prefix][label] /= total

    @timer
    @memory
    def predict(self, data: 'EventLogHandler') -> 'AbstractPrediction':
        prediction = {}

        for i, trace in enumerate(data.dataset.traces):

            prediction[i] = {}
            for n in range(1, self._n + 1):
                prefix = Utils.list_to_string([event.identifier for event in trace.events[-n:]])
                if n < len(trace) and not isinstance(trace.events[-1], EndEvent):
                    prediction[i][prefix] = self._gram_to_probability[n][prefix]

        return prediction

    def evaluate(self, data: 'EventLogHandler') -> 'AbstractMetric':
        pass

    def save(self, path: typing.Union[str, 'Path']):
        pass

    def load_weights(self, path: typing.Union[str, 'Path']):
        pass
