import datetime
import logging

import pandas as pd

from data.handler.EventLogHandler import EventLogHandler
from data.mining.MiningUtils import MiningUtils
from ml.classifier.NGram import NGram
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intercase.AverageDelay import AverageDelay
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)

# event log in den handler laden
data_handler = EventLogHandler()
data_handler.load(str(Path(['resources', '20220105_test_tokens.xes'])), loader_kwargs={'wrap_trace': False})
data_handler.process_definition = MiningUtils.process_definition_from_event_log(data_handler.dataset)
data_handler.mine_performance_spectrum()

data_handler.time_series = MiningUtils.time_series_from_event_log(data_handler.dataset)
data_handler.mine_batching_patterns(min_batch_size=5, max_delay=datetime.timedelta(hours=3))

ngram = NGram(n=3)
ngram.fit(data_handler)

predictions = ngram.predict(data_handler)

timedeltas = [pd.Timedelta(days=3), pd.Timedelta(days=2), pd.Timedelta(days=3)]

for timedelta in timedeltas:
    builder = EventLogEncodingBuilder() \
        .add(AverageDelay(timeframe=timedelta))
    #    .add(NoPeerCases(timeframe=timedelta)) \
    #    .add(PeerActivityCount(timeframe=timedelta)) \
    #    .add(FrequentPreviousActivity(timeframe=timedelta, no_activities=2)) \
    #    .add(ResourceCount(timeframe=timedelta)) \
    #    .add(TopBusyResource(timeframe=timedelta, no_resources=2)) \
    #    .add(FutureBatchingBehaviour(segment_classifier=ngram)) \

    features, labels = builder.build(data_handler)
    import matplotlib.pyplot as plt
    plt.plot(list(map(lambda x: x[0].days, features)))
    plt.show()
pass
# encoding = AggregationEncoderDepr().encode(data_handler)

# classifier = xgb.XGBClassifier()
# classifier = RandomForestClassifier()
# classifier = SVC()
#
# classifier.fit(features, labels)
