import logging
import sys

from sklearn.base import ClassifierMixin, BaseEstimator

try:
    from jax.config import config

    config.update("jax_enable_x64", True)
    import jax
    import optax
    from jax import numpy as jnp
except ImportError as e:
    logging.warning('Jaxlib not found during init of QKE. Use pip install jaxlib to enable circuit compilation!')

import numpy as np
import pennylane as qml
from pennylane import numpy as pnp, AdamOptimizer

from ml.MLUtils import MLUtils
from ml.classifier.quantum.QuantumEmbedding import QuantumEmbedding
from ml.classifier.quantum.QuantumLayer import QuantumLayer
from ml.metrics.MetricFunctions import MetricFunctions
from ml.metrics.VectorMetric import VectorMetric


class VQC(BaseEstimator, ClassifierMixin):
    def __init__(self,
                 state_preparation=QuantumEmbedding.angle,
                 layer=QuantumLayer.default_variational,
                 optimizer=optax.adam(learning_rate=0.05),
                 cost_fn=MetricFunctions.jax_square_loss,
                 n_layers: int = 2,
                 min_wires: int = 4,
                 batch_size: int = 16,
                 epochs: int = 100,
                 dev=None,
                 use_jax: bool = False,
                 debug: bool = False,
                 **kwargs):

        super().__init__(**kwargs)
        self.model = None
        self.weights = None
        self.bias = None

        self.state_preparation = state_preparation
        self.layer = layer
        self.n_layers = n_layers
        self.min_wires = min_wires
        self.batch_size = batch_size
        self.epochs = epochs

        self.dev = dev
        self.optimizer = optimizer
        self.cost_fn = cost_fn

        self.use_jax = use_jax
        self.interface = 'jax' if self.use_jax else 'autograd'

        self.debug = debug
        self.is_trained = False

    def _variational_classifier(self, w, x, b):

        @qml.qnode(self.dev, interface=self.interface)
        def _circuit(x_, w_):
            assert len(w_) >= 2
            for i_ in range(len(w_)):
                if i_ != 0:
                    self.state_preparation(x_, n_wires=self._n_wires)
                self.layer(w_[i_])
            return tuple([qml.expval(qml.PauliZ(wires=i_)) for i_ in range(self._n_wires)])

        circuit = _circuit if not self.use_jax else jax.jit(_circuit)
        return circuit(x, w) + b

    def _cost(self, weights, bias, features, labels):
        predictions = [self._variational_classifier(weights, x, bias) for x in features]
        return self.cost_fn(labels, predictions)

    def fit(self, X: np.ndarray, y: np.ndarray) -> 'VQC':
        # Determine number of wires needed, next higher integer to the power of two for y, min number of features
        min_wires_x = len(X[0])
        self._n_wires = max(min_wires_x, self.min_wires)

        # Make features and labels differentiable
        x_train = pnp.array(X, requires_grad=False)
        y_train = pnp.zeros(shape=(len(y), self._n_wires), requires_grad=False)

        # Transform labels to encoding that matches the quantum computer with respective number of wires
        for i in range(len(y)):
            y_train[i] = MLUtils.float_to_binary_array(y[i], self._n_wires)

        # Automatically set device using determined number of wires
        if self.dev is None:
            self.dev = qml.device("default.qubit", wires=self._n_wires)

        # Initialise weights and bias if not trained before, classifier can be retrained
        if self.weights is None:
            self.weights = 0.01 * np.random.randn(self.n_layers, self._n_wires, 3)
            self.weights = jnp.asarray(self.weights)
        if self.bias is None:
            self.bias = jnp.zeros(self._n_wires)

        # Iterate for some epochs and optimize the variational model
        n_train_samples = len(x_train)

        params = {'w': self.weights, 'b': self.bias}
        opt_state = self.optimizer.init(params)

        def cost_(p_, x_, y_):
            predictions = [self._variational_classifier(p_['w'], feat_, p_['b']) for feat_ in x_]
            return self.cost_fn(y_, predictions)

        for i_ in range(self.epochs):
            # Create a random batch of samples
            batch_index = pnp.random.randint(0, n_train_samples, (self.batch_size,))
            x_train_batch = x_train[batch_index]
            y_train_batch = y_train[batch_index]


            cost, grad_circuit = jax.value_and_grad(lambda p: cost_(p, x_train_batch, y_train_batch))(params)
            updates, opt_state = self.optimizer.update(grad_circuit, opt_state)
            params = optax.apply_updates(params, updates)

            if self.debug:
                sys.stdout.write(f'\rVQE -> Epoch:{i_}')
                sys.stdout.flush()

        sys.stdout.write('\n')
        self.is_trained = True

        return self

    def predict(self, X: np.ndarray) -> np.ndarray:
        prediction = [self._variational_classifier(self.weights, x, self.bias) for x in X]
        return np.array([MLUtils.binary_array_argmax(p) for p in prediction])

    def score(self, X: np.ndarray, y: np.ndarray, sample_weight=None) -> float:
        prediction = [self._variational_classifier(self.weights, x, self.bias) for x in X]
        y_pred = np.array([MLUtils.binary_array_argmax(p) for p in prediction])

        return VectorMetric(y, y_pred).accuracy()
