import logging
import sys
import time
from typing import TYPE_CHECKING

try:
    from jax.config import config

    config.update("jax_enable_x64", True)
    import jax
except ImportError as e:
    logging.warning('Jaxlib not found during init of QKE. Use pip install jaxlib to enable circuit compilation!')

import pennylane as qml
from pennylane import numpy as pnp
from sklearn.svm import SVC

from data.handler.EventLogHandler import EventLogHandler
from ml import AbstractPrediction
from ml.AbstractClassifier import AbstractClassifier
from ml.AbstractMetric import AbstractMetric
from ml.classifier.quantum.QuantumEmbedding import QuantumEmbedding
from ml.metrics.VectorMetric import VectorMetric
from utils.Decorators import timer, memory
from utils.Enums import PredictionType

if TYPE_CHECKING:
    from ml.encoding.BuilderConfiguration import BuilderConfiguration


class QKE(AbstractClassifier):
    def __init__(self, kernel=QuantumEmbedding.angle, n_layers: int = 2,
                 n_wires: int = 2, dev=qml.device("default.qubit", wires=2), jax: bool = False, debug: bool = False,
                 **kwargs):

        from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
        from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder

        super().__init__(**kwargs)
        self.model = None
        self._debug = debug

        self._jax = jax
        self._interface = 'jax' if self._jax else None

        self._kernel_function = kernel
        self._n_layers = n_layers
        self._n_wires = n_wires

        self._dev = dev
        self._x_train = pnp.array([])

        self.prediction_type = PredictionType.ACTIVITY
        if self.encoder is None:
            self.encoder = EventLogEncodingBuilder() \
                .add(IndexBasedEncoder(window=self._n_wires, normalization=True))

    def _kernel_matrix(self, A, B):
        """Compute the matrix whose entries are the kernel
           evaluated on pairwise data from sets A and B."""

        projector_ = pnp.zeros((2 ** self._n_wires, 2 ** self._n_wires))
        projector_[0, 0] = 1

        @qml.qnode(self._dev, interface=self._interface)
        def _kernel_embedding(x, y):
            self._kernel_function(x, n_layers=self._n_layers, n_wires=self._n_wires)
            adj_ansatz = qml.adjoint(self._kernel_function)
            adj_ansatz(y, n_layers=self._n_layers, n_wires=self._n_wires)
            return qml.expval(qml.Hermitian(projector_, wires=range(self._n_wires)))

        circuit_ = _kernel_embedding if not self._jax else jax.jit(_kernel_embedding)

        total_len = len(B) * len(A)
        counter = 0
        arr = pnp.empty(shape=(len(A), len(B)))
        start_time = time.time()
        for i, a in enumerate(A):
            for j, b in enumerate(B):
                arr[i, j] = circuit_(a, b)
                if self._debug:
                    time_diff = time.time() - start_time
                    sys.stdout.write(
                        f'\r{counter}/{total_len} : {(100 * counter / total_len):.3f}% : time {int(time_diff / 60)}min')
                    sys.stdout.flush()
                counter += 1
        sys.stdout.write('\n')
        return arr

    @timer
    @memory
    def fit(self, train_handler: 'EventLogHandler', val_handler: 'EventLogHandler' = None):
        x_train, y_train = self.encoder.build(train_handler)

        self._n_wires = len(x_train[0])

        # If amplitude encoding, n_qubits = log_2(n_features)
        # self._n_wires = np.int64(np.ceil(np.log2(n_features))) \
        #     if self._kernel_function == QuantumKernel.angle_embedding \
        #     else n_features

        self._dev = qml.device("default.qubit", wires=self._n_wires)
        self._x_train = x_train
        matrix_train = self._kernel_matrix(x_train, x_train)

        self.model = SVC(kernel='precomputed')
        self.model.fit(matrix_train, y_train)

    @timer
    @memory
    def predict(self, predict_handler: 'EventLogHandler',
                builder_configuration: 'BuilderConfiguration' = None) -> 'AbstractPrediction':
        x_predict, _ = self.encoder.build(predict_handler, builder_configuration=builder_configuration, predict=True)

        matrix_predict = self._kernel_matrix(x_predict, self._x_train)
        prediction = self.model.predict(matrix_predict)

        return prediction

    def evaluate(self, val_handler: 'EventLogHandler') -> 'AbstractMetric':
        x_val, y_val = self.encoder.build(val_handler)

        matrix_val = self._kernel_matrix(x_val, self._x_train)
        prediction = self.model.predict(matrix_val)

        return VectorMetric(y_val, prediction)
