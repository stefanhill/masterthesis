import pennylane as qml
from matplotlib import pyplot as plt
from pennylane import numpy as np, QNGOptimizer
from sklearn.model_selection import train_test_split

from data.handler.EventLogHandler import EventLogHandler
from data.mining.MiningUtils import MiningUtils
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

NO_WIRES = 4
NO_LAYERS = 4
NO_SHOTS = 10

dev = qml.device('default.qubit', wires=NO_WIRES)


def statepreparation(x, rotation_type):
    i_ = 0
    for x_ in x:
        if rotation_type % 3 == 1:
            qml.RY(x_, wires=i_ % NO_WIRES)
        elif rotation_type % 3 == 2:
            qml.RX(x_, wires=i_ % NO_WIRES)
        else:
            qml.RZ(x_, wires=i_ % NO_WIRES)


def layer(w):
    i_ = 0
    for w_ in w:
        qml.CRot(w_[0], w_[1], w_[2], wires=[i_ % NO_WIRES, (i_ + 1) % NO_WIRES])


def binary_list_to_int(binary_list):
    r = 0
    i_ = 0
    for elem_ in binary_list:
        r += elem_ ** i_
        i_ += 1
    return r


def decode_pauli_to_class():
    probs = np.zeros(NO_WIRES ** 2)
    for _ in range(NO_SHOTS):
        sample = qml.sample()
        idx_ = binary_list_to_int(sample)
        probs[idx_] = probs[idx_] + 1
    probs = probs / NO_SHOTS
    return tuple(probs)


@qml.qnode(dev)
def circuit(x, w):
    assert len(w) >= 2
    for i_ in range(len(w)):
        if i_ != 0:
            statepreparation(x, 1)
        layer(w[i_])
    return tuple([qml.expval(qml.PauliZ(wires=i_)) for i_ in range(NO_WIRES)])


def variational_classifier(w, b, x):
    return circuit(x, w) + b


def square_loss(labels, predictions):
    loss = 0
    for l, p in zip(labels, predictions):
        loss = loss + np.dot((l - p), (l - p).T)

    loss = loss / len(labels)
    return loss


def accuracy(labels, predictions):
    loss = 0
    for l, p in zip(labels, predictions):
        correct_ = len(p) > 0
        for l_i, p_i in zip(l, p):
            correct_ = correct_ and (round(float(l_i)) == round(float(p_i)))
        if correct_:
            loss = loss + 1
    loss = loss / len(labels)

    return loss


def cost(weights, bias, features, labels):
    predictions = [variational_classifier(weights, bias, f) for f in features]
    return square_loss(labels, predictions)


# def cost(weights, features, labels):
#     predictions = [variational_classifier(weights, f) for f in features]
#     return square_loss(labels, predictions)


# event log in den handler laden
data_handler = EventLogHandler()
data_handler.load(str(Path(['resources', '20220105_test_tokens.xes'])))
data_handler.process_definition = MiningUtils.process_definition_from_event_log(data_handler.dataset)
performance_spectrum = MiningUtils.performance_spectrum_from_event_log(data_handler.dataset)

data_handler.time_series = MiningUtils.time_series_from_event_log(data_handler.dataset)

builder = EventLogEncodingBuilder() \
    .add(IndexBasedEncoder(window=2, normalization=True))

X, Y_temp = builder.build(data_handler)


# TODO: maybe move in later if optimizer cannot handle 0
# X[X == 0.0] = 0.0001


def float_to_binary_array(value, dim):
    r = np.zeros(dim)
    j_ = 1
    for i_ in range(dim):
        if value % j_ == 0:
            r[i_] = 1
        j_ = j_ * 2
    return r[::-1]


# Y = Y_temp

# TODO: important, because otherwise optimizer tries to optimize X and Y as well
X = np.array(X, requires_grad=False)
Y = np.zeros(shape=(len(Y_temp), NO_WIRES), requires_grad=False)
for i in range(len(Y_temp)):
    Y[i] = float_to_binary_array(Y_temp[i], NO_WIRES)

# TODO: number of wires must be determined automatically by the number of wires

np.random.seed(0)
num_data = len(Y)
num_train = int(0.75 * num_data)
index = np.random.permutation(range(num_data))
X_train, X_val, Y_train, Y_val = train_test_split(X, Y, test_size=0.2, random_state=3)

num_train = len(X_train)

weights_init = 0.01 * np.random.randn(NO_LAYERS, NO_WIRES, 3, requires_grad=True)
bias_init = np.zeros(NO_WIRES, requires_grad=True)

# TODO: move to another file and create a variational classifier using qng
opt = QNGOptimizer(0.05)
# opt = AdamOptimizer(0.05)
batch_size = 32

# train the variational classifier
weights = weights_init
bias = bias_init

for i_layer in [2, 3, 4]:
    costs = []
    accuracies = []

    for iteration in range(2):
        for it in range(32):
            # Update the weights by one optimizer step
            batch_index = np.random.randint(0, num_train, (batch_size,))
            feats_train_batch = X_train[batch_index]
            Y_train_batch = Y_train[batch_index]
            # https://discuss.pennylane.ai/t/variational-classifiers-and-qngoptimizer/524/11

            obs = [qml.expval(qml.PauliZ(wires=i_)) for i_ in range(NO_WIRES)]

            H = qml.Hamiltonian(Y_train_batch, obs)
            cost_fn = qml.ExpvalCost(circuit, H, dev)

            weights, bias, _, _ = opt.step(cost, weights, feats_train_batch, Y_train_batch)
            # weights, _, _ = opt.step(cost, weights, feats_train_batch, Y_train_batch)
            # weights, _, _ = opt.minimize(cost, var_list=[weights, feats_train_batch, Y_train_batch])

            predictions_batch = [variational_classifier(weights, bias, f) for f in feats_train_batch]
            acc_batch = accuracy(Y_train_batch, predictions_batch)
            sq_loss = square_loss(Y_train_batch, predictions_batch)
            costs.append(sq_loss)
            accuracies.append(acc_batch)

            print(
                "Iter: {:5d} | Cost: {:0.7f} | Acc batch: {:0.7f}"
                "".format(it + 1, sq_loss, acc_batch)
            )

        # TODO: acc and loss for complete dataset
        # Compute predictions on train and validation set
        # predictions_train = [variational_classifier(weights, bias, f) for f in X_train]
        # predictions_val = [variational_classifier(weights, bias, f) for f in X_val]

        # Compute accuracy on train and validation set
        # acc_train = accuracy(Y_train, predictions_train)
        # acc_val = accuracy(Y_val, predictions_val)

        # c = cost(weights, bias, X_val, Y_val)

        # print(
        #     "Validation {:5d} | Cost validation: {:0.7f} | Acc train: {:0.7f} | Acc validation: {:0.7f} "
        #     "".format(iteration, c, acc_train, acc_val)
        # )

    plt.plot(costs)
    plt.plot(accuracies)
    plt.show()
