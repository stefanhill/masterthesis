import logging

import pandas as pd
from sklearn.svm import SVC

from data.handler.EventLogHandler import EventLogHandler
from data.mining.MiningUtils import MiningUtils
from data.processing.filter.TraceFilter import TraceFilter
from data.processing.preprocessing.TraceCut import TraceCut
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intercase.NoPeerCases import NoPeerCases
from ml.encoding.intracase.AggregationEncoder import AggregationEncoder
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)

# event log in den handler laden
data_handler = EventLogHandler()
data_handler.load(str(Path(['resources', '20220105_test_tokens.xes'])))
data_handler.process_definition = MiningUtils.process_definition_from_event_log(data_handler.dataset)
performance_spectrum = MiningUtils.performance_spectrum_from_event_log(data_handler.dataset)

data_handler.time_series = MiningUtils.time_series_from_event_log(data_handler.dataset)


# reference_event = data_handler.dataset.traces[200].events[1]
#
# window = time_series[(time_series.index > reference_event.timestamp - pd.Timedelta(days=1)) & (
#             time_series.index < reference_event.timestamp)]
#
# peer_cases = set(map(lambda x: x[0], window))

builder = EventLogEncodingBuilder() \
    .add(AggregationEncoder()) \
    .add(NoPeerCases(timeframe=pd.Timedelta(days=1)))

features, labels = builder.build(data_handler)
#encoding = AggregationEncoderDepr().encode(data_handler)

# classifier = xgb.XGBClassifier()
# classifier = RandomForestClassifier()
classifier = SVC()

classifier.fit(features, labels)

case_id_frontend = '174084'
running_instance = data_handler.get(TraceFilter(attribute_name='concept:name', filter_values=[case_id_frontend]))
running_instance.process([TraceCut(cut_position=6)])
# running_instance = data_handler.get(RandomTraces(no_traces=1, min_trace_length=10))
# running_instance.process([RandomTraceCut(min_trace_length=5)])

# last_event = running_instance.dataset.instances[0].events.pop()

running_instance.process_definition = data_handler.process_definition
running_instance.time_series = MiningUtils.time_series_from_event_log(running_instance)

r_features, r_labels = builder.build(running_instance)
prediction = classifier.predict(r_features)

# objekt mit ergebnissen
"""
    case-id1: {
        1-gram: {
            event1: prob1,
            event2: prob2
            ...
        },
        ...,
        n-gram: {
            event1: prob1,
            ...
        }
    }
    case-id2: ...
"""
print(prediction, r_labels)
