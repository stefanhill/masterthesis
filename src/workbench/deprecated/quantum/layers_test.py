import datetime
import logging

from data.handler.EventLogHandler import EventLogHandler
from ml.classifier.VQC import VQC
from ml.classifier.quantum.QuantumEmbedding import QuantumEmbedding
from ml.classifier.quantum.QuantumLayer import QuantumLayer
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)

timedelta = datetime.timedelta(days=1)

train_handler = EventLogHandler()
train_handler.load(str(Path(['resources', 'test_logs', 'bpi2011_train.xes'])))

val_handler = EventLogHandler()
val_handler.load(str(Path(['resources', 'test_logs', 'bpi2011_validation.xes'])))

# Ensure the mapping is the same for train and validation handler
val_handler.process_definition = train_handler.process_definition

train_handler.mine_batching_patterns(min_batch_size=5, max_delay=datetime.timedelta(hours=3))
val_handler.mine_batching_patterns(min_batch_size=5, max_delay=datetime.timedelta(hours=3))

intra_case_encoder = EventLogEncodingBuilder() \
    .add(IndexBasedEncoder(window=4, normalization=True))

vqc = VQC(n_layers=2, encoder=intra_case_encoder, state_preparation=QuantumEmbedding.zz_angle,
            layer=QuantumLayer.default_variational)
vqc.fit(train_handler)

acc = vqc.evaluate(val_handler).accuracy()
