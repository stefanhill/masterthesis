
import pickle

import numpy as np
import pandas as pd
from ortools.constraint_solver import pywrapcp
from ortools.constraint_solver import routing_enums_pb2

vehicle_df = pd.read_csv('vehicle_information.csv', delimiter=';')

demand_df = pd.read_csv('delivery_weights_during_peak.csv')
demand_df = demand_df.drop(['Unnamed: 0'], axis=1)

# We take the maximal demand to find some meaningful route combinations
max_demand = max(demand_df.sum()[2:])
# %%
distance_df = pd.read_excel('Project_Part_II.xlsx', sheet_name='Distance_matrix', header=1)
distance_df = distance_df.drop(['Unnamed: 0'], axis=1).to_numpy()
# %%
cap_info = vehicle_df[['vehicle_no', 'capacity']].values

# Add the maximal number of vehicles needed to fulfull the demand
max_no_vehicles = []
for vehicle_ in cap_info:
    i_ = 0
    while i_ * vehicle_[1] < max_demand:
        i_ += 1
    max_no_vehicles.append(i_)

# Add as a column to the dataframe
vehicle_df.insert(len(vehicle_df.columns), 'max_no_vehicles', max_no_vehicles)
# %%
# We have to assure that the dataframe is ordered by capacity descendingly
vehicle_df = vehicle_df.sort_values('capacity', ascending=False).reset_index(drop=True)
vehicle_df.insert(len(vehicle_df.columns), 'used_in_solution', vehicle_df['max_no_vehicles'].values)
# %%
vehicle_solutions = []


def find_vehicle_combination_rec(start_index_, max_demand_, vehicle_df_, current_vehicle_solution_):
    """
    Find the combinations of vehicles in a recursive style
    Args:
      start_index_: id of vehicle to check if should be added
      max_demand_: maximal demand, we a fleet that fulfulls the demand, but not more
      vehicle_df_: the dataframe, we take 'used_in_solution' column to store recursive information
      current_vehicle_solution_: a list of vehicle numbers to be added to the solution

    Returns: None, but solutions are written into a list in the outer scope

    """

    def get_sum(vehicle_solution_):
        s = 0
        for i_ in vehicle_solution_:
            s += vehicle_df.loc[i_]['capacity']
        return s

    if get_sum(current_vehicle_solution_) >= max_demand_:
        vehicle_solutions.append(current_vehicle_solution_)
        return

    if start_index_ > max(vehicle_df_.index):
        return

    new_current_vehicle_solution_ = current_vehicle_solution_ + [start_index_]
    new_vehicle_df_ = vehicle_df_.copy(deep=True)
    new_vehicle_df_.at[start_index_, 'used_in_solution'] = int(new_vehicle_df_['used_in_solution'][start_index_]) - 1

    find_vehicle_combination_rec(start_index_, max_demand_, new_vehicle_df_, new_current_vehicle_solution_)
    find_vehicle_combination_rec(start_index_ + 1, max_demand_, vehicle_df, current_vehicle_solution_)


# We initatialize our greedy search with the biggest capacity vehicle, the max demand, the dataframe and an empty solution
find_vehicle_combination_rec(0, max_demand, vehicle_df, [])
print(vehicle_solutions)


# %%
# [START data_model]
def create_data_model(fleet_, day_):
    # TODO: this is only temporary, we have to iterate over the dates
    Demand = demand_df[['ID', day_]].to_numpy()

    Distance = distance_df
    # Deletes the first rows from the distance matrix, because we do not need them
    Distance = np.delete(Distance, 0, 1)

    # Student input 3: Enter the hub number for variable Hub, DO NOT update variable Hub_updated.

    # Askim is ID number 4
    Hub = int(16)  # Defines hub

    # Please do not update this one

    # Hub is always treated as the first value in the demand matrix
    Hub_updated = int(0)  # Updates hub
    List_nodes = []
    temp = 0

    # Iterates over the demands and counts how many nodes have non-zero weights
    for i in range(0, np.size(Demand, 0)):  # Updates distance matrix
        if i == Hub or Demand[i, 1] > 0:
            temp = temp + 1
            List_nodes.append(i)

    # newly shaped demand and distance matrix
    Distance_updated = np.empty([temp, temp], dtype=int)
    Demand_updated = np.empty([temp, 2], dtype=int)

    # Updates the distance matrix -> useful
    for i in range(0, temp):
        for j in range(0, temp):
            Distance_updated[i, j] = Distance[List_nodes[i], List_nodes[j]]

    # Updates the demand
    for i in range(0, temp):
        Demand_updated[i, 0] = Demand[List_nodes[i], 0]
        Demand_updated[i, 1] = Demand[List_nodes[i], 1]
        if List_nodes[i] == Hub:
            Hub_updated = i

    # Set the demand at the hub equal to zero
    Demand_updated[Hub_updated, 1] = 0  # Update Demand at hub=0
    del temp, Demand, Distance, Hub

    # END DATA PROCESSING

    """Stores the data for the problem."""
    data = {}

    # [START demands_capacities]
    # TODO: Here, we load the demands dynamically from the demand dataset via day_
    # TODO: Check for 0 demands and remove
    data['distance_matrix'] = Distance_updated.tolist()

    # [START demands_capacities]
    temp = Demand_updated
    data['demands'] = []
    for i in range(0, np.size(temp, 0)):
        data['demands'].append(temp[i, 1])
    del temp
    data['depot'] = Hub_updated
    data['node_map'] = List_nodes

    data['vehicle_capacities'] = [vehicle_df['capacity'][i_] for i_ in fleet_]
    # [END demands_capacities]
    data['num_vehicles'] = len(fleet_)

    return data
    # [END data_model]


# %%
# TODO: we do not need the solution printer excactly, but, we use it to create views of different solutions

# [START solution_printer]
def print_solution(data, manager, routing, solution):
    """Prints solution on console."""
    print(f'Objective: {solution.ObjectiveValue()} m')
    total_distance = 0
    total_load = 0
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        plan_output = 'Route for vehicle {}:\n'.format(vehicle_id)
        route_distance = 0
        route_load = 0
        while not routing.IsEnd(index):
            node_index = manager.IndexToNode(index)
            route_load += data['demands'][node_index]
            plan_output += ' {0} Load({1}) -> '.format(data['node_map'][node_index], route_load)
            previous_index = index
            index = solution.Value(routing.NextVar(index))
            route_distance += routing.GetArcCostForVehicle(
                previous_index, index, vehicle_id)
        plan_output += ' {0} Load({1})\n'.format(data['node_map'][manager.IndexToNode(index)],
                                                 route_load)
        plan_output += 'Distance of the route: {}m\n'.format(route_distance)
        plan_output += 'Load of the route: {}\n'.format(route_load)
        print(plan_output)
        total_distance += route_distance
        total_load += route_load
    print('Total distance of all routes: {}m'.format(total_distance))
    print('Total load of all routes: {}'.format(total_load))
    # [END solution_printer]


# %%
def get_route_information(data, manager, routing, solution):
    route_data = {}
    route_data['objective_value'] = solution.ObjectiveValue()
    route_data['routes'] = {}

    total_distance = 0
    total_load = 0
    for vehicle_id in range(data['num_vehicles']):
        route_ = {
            'nodes': [],
            'loads': [],
            'loads_agg': []
        }
        index = routing.Start(vehicle_id)
        route_distance = 0
        route_load = 0
        while not routing.IsEnd(index):
            node_index = manager.IndexToNode(index)
            route_load += data['demands'][node_index]
            route_['nodes'].append(data['node_map'][node_index])
            route_['loads'].append(data['demands'][node_index])
            route_['loads_agg'].append(route_load)
            previous_index = index
            index = solution.Value(routing.NextVar(index))
            route_distance += routing.GetArcCostForVehicle(
                previous_index, index, vehicle_id)
        route_['nodes'].append(data['node_map'][manager.IndexToNode(index)])
        route_['loads'].append(0)
        route_['loads_agg'].append(route_load)
        route_['route_distance'] = route_distance
        total_distance += route_distance
        total_load += route_load
        route_data['routes'][vehicle_id] = route_

    route_data['total_distance'] = total_distance
    route_data['total_load'] = total_load
    return route_data


# %%
def solve_cvrp(fleet_, day_):
    """Solve the CVRP problem."""
    # Instantiate the data problem.
    # [START data]
    data = create_data_model(fleet_, day_)
    # [END data]

    # Create the routing index manager.
    # [START index_manager]
    manager = pywrapcp.RoutingIndexManager(len(data['distance_matrix']), data['num_vehicles'], data['depot'])
    # [END index_manager]

    # Create Routing Model.
    # [START routing_model]
    routing = pywrapcp.RoutingModel(manager)

    # [END routing_model]

    # Create and register a transit callback.
    # [START transit_callback]
    def distance_callback(from_index, to_index):
        """Returns the distance between the two nodes."""
        # Convert from routing variable Index to distance matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['distance_matrix'][from_node][to_node]

    transit_callback_index = routing.RegisterTransitCallback(distance_callback)
    # [END transit_callback]

    # Define cost of each arc.
    # [START arc_cost]
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

    # [END arc_cost]

    # Add Capacity constraint.
    # [START capacity_constraint]
    def demand_callback(from_index):
        """Returns the demand of the node."""
        # Convert from routing variable Index to demands NodeIndex.
        from_node = manager.IndexToNode(from_index)
        return data['demands'][from_node]

    demand_callback_index = routing.RegisterUnaryTransitCallback(
        demand_callback)
    routing.AddDimensionWithVehicleCapacity(
        demand_callback_index,
        0,  # null capacity slack
        data['vehicle_capacities'],  # vehicle maximum capacities
        True,  # start cumul to zero
        'Capacity')
    # [END capacity_constraint]

    # Setting first solution heuristic.
    # [START parameters]
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)
    search_parameters.local_search_metaheuristic = (
        routing_enums_pb2.LocalSearchMetaheuristic.GUIDED_LOCAL_SEARCH)
    search_parameters.time_limit.FromSeconds(1)
    # [END parameters]

    # Search strategy
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.local_search_metaheuristic = (routing_enums_pb2.LocalSearchMetaheuristic.GUIDED_LOCAL_SEARCH)
    search_parameters.time_limit.seconds = 30
    search_parameters.log_search = True

    # Solve the problem.
    # [START solve]
    solution = routing.SolveWithParameters(search_parameters)
    # [END solve]

    if solution:
        print_solution(data, manager, routing, solution)
        return get_route_information(data, manager, routing, solution)


# %%
cvrp_solutions = {}

from functools import reduce

# Read the days in the demand dataframe
days = demand_df.columns[2:].to_list()


# days = ['2019-11-11']

def fleet_to_str(fleet_):
    return reduce(lambda a, b: f'{a}+{b}', fleet_)


for fleet in vehicle_solutions:
    fleet_key = fleet_to_str(fleet)
    cvrp_solutions[fleet_key] = {}
    print('\n Fleet combination: ', fleet_key)
    # TODO: add the days for the dataframe here
    for day in days:
        print('Day: ', day)
        cvrp_solutions[fleet_key][day] = solve_cvrp(fleet, day)


pickle.dump(cvrp_solutions, open('solutions_during_peak.p', 'wb'), protocol=pickle.HIGHEST_PROTOCOL)
vehicle_df.to_csv('fleet_information.csv')
