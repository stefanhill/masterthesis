from data.handler.EventLogHandler import EventLogHandler
from ml.classifier.QKE import QKE
from ml.classifier.quantum.QuantumEmbedding import QuantumEmbedding
from utils.Path import Path

data_handler = EventLogHandler()
data_handler.load(str(Path(['resources', '20220105_test_tokens.xes'])), loader_kwargs={'wrap_trace': False})

# TODO: currently, number of instances is cut off inside qke.fit(), implement methods for train test split and sampling

qke = QKE(n_layers=2, kernel=QuantumEmbedding.zz)
qke.fit(data_handler)

prediction = qke.predict(data_handler)
