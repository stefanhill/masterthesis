import logging

from data.handler.EventLogHandler import EventLogHandler
from ml.classifier.NGram import NGram
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)

data_handler = EventLogHandler()
data_handler.load(str(Path(['resources', '20220105_test_tokens.xes'])), loader_kwargs={'wrap_trace': False})

# TODO: currently, number of instances is cut off inside qke.fit(), implement methods for train test split and sampling

encoder = EventLogEncodingBuilder() \
    .add(IndexBasedEncoder(window=4, normalization=True))

ngram = NGram(n=4)
ngram.fit(data_handler)
metric = ngram.evaluate(data_handler)
