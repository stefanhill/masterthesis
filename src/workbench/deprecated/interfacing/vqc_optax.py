import sys
sys.path.append('/home/david/masterthesis-quantum-ppm/masterthesis/src/')

from ml.MLUtils import MLUtils


from data.handler.EventLogHandler import EventLogHandler
from ml.classifier.VQC import VQC
from ml.classifier.quantum.QuantumEmbedding import QuantumEmbedding
from ml.classifier.quantum.QuantumLayer import QuantumLayer
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

handler = EventLogHandler()
handler.load(str(Path(['resources', 'test_logs', 'bpi2011_train.xes'])))

builder = EventLogEncodingBuilder() \
    .add(IndexBasedEncoder(window=4))

X, y = builder.build(handler)

vqc = VQC(state_preparation=QuantumEmbedding.angle,
          layer=QuantumLayer.default_variational,
          use_jax=False,
          debug=True,
          min_wires=MLUtils.next_2_pot(len(handler.process_definition)))

vqc.fit(X, y)
acc = vqc.score(X, y)
print(acc)
