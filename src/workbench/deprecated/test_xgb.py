import logging

from ml.encoding.AggregationEncoderDepr import AggregationEncoderDepr
from sklearn.svm import SVC

from data.handler.EventLogHandler import EventLogHandler
from data.mining.MiningUtils import MiningUtils
from data.processing.filter.TraceFilter import TraceFilter
from data.processing.preprocessing.TraceCut import TraceCut
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)

# event log in den handler laden
data_handler = EventLogHandler()
data_handler.load(str(Path(['resources', '20220105_test_tokens.xes'])))
data_handler.process_definition = MiningUtils.process_definition_from_event_log(data_handler.dataset)
performance_spectrum = MiningUtils.performance_spectrum_from_event_log(data_handler.dataset)

encoding = AggregationEncoderDepr().encode(data_handler)

#classifier = xgb.XGBClassifier()
#classifier = RandomForestClassifier()
classifier = SVC()

classifier.fit(encoding[0], encoding[1])

# @Ali: interface für ein dataset für die prediction
case_id_frontend = '174084'
running_instance = data_handler.get(TraceFilter(attribute_name='concept:name', filter_values=[case_id_frontend]))
running_instance.process([TraceCut(cut_position=6)])
#running_instance = data_handler.get(RandomTraces(no_traces=1, min_trace_length=10))
#running_instance.process([RandomTraceCut(min_trace_length=5)])

#last_event = running_instance.dataset.instances[0].events.pop()

running_instance.process_definition = data_handler.process_definition
running_encoding = AggregationEncoderDepr().encode(running_instance)
prediction = classifier.predict(running_encoding[0])


# objekt mit ergebnissen
"""
    case-id1: {
        1-gram: {
            event1: prob1,
            event2: prob2
            ...
        },
        ...,
        n-gram: {
            event1: prob1,
            ...
        }
    }
    case-id2: ...
"""
print(prediction, running_encoding[1])