import pennylane as qml

dev = qml.device('default.qubit', wires=1)


@qml.qnode(dev)
def circuit(params):
    qml.RX(params[0], wires=0)
    qml.RY(params[1], wires=0)
    return qml.expval(qml.PauliZ(wires=0))


print(circuit([0.54, 0.12]))
