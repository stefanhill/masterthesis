import pennylane as qml
from pennylane import numpy as np, AdamOptimizer
from sklearn.model_selection import train_test_split

from data.handler.EventLogHandler import EventLogHandler
from data.mining.MiningUtils import MiningUtils
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

NO_WIRES = 4
NO_LAYERS = 2
NO_SHOTS = 10

dev = qml.device('default.qubit', wires=NO_WIRES)


def statepreparation(x):
    i_ = 0
    for x_ in x:
        qml.RY(x_, wires=i_ % NO_WIRES)


def layer(w):
    i_ = 0
    for w_ in w:
        qml.CRot(w_[0], w_[1], w_[2], wires=[i_ % NO_WIRES, (i_+1) % NO_WIRES])

def binary_list_to_int(binary_list):
    r = 0
    i_ = 0
    for elem_ in binary_list:
        r += elem_**i_
        i_ += 1
    return r

def decode_pauli_to_class():
    probs = np.zeros(NO_WIRES**2)
    for _ in range(NO_SHOTS):
        sample = qml.sample()
        idx_ = binary_list_to_int(sample)
        probs[idx_] = probs[idx_] + 1
    return probs / NO_SHOTS


@qml.qnode(dev)
def circuit(x, w):
    statepreparation(x)
    for w_ in w:
        layer(w_)
        statepreparation(x)

    # TODO: try to reverse engineer the amplitude into class vector
    return qml.expval(qml.PauliY(wires=[0]))


def variational_classifier(w, x):
    return circuit(x, w)

# def variational_classifier(w, b, x):
#     return circuit(x, w) + b
#

def square_loss(labels, predictions):
    loss = 0
    for l, p in zip(labels, predictions):
        loss = loss + np.dot((l - p), (l - p).T)

    loss = loss / len(labels)
    return loss


def accuracy(labels, predictions):
    loss = 0
    for l, p in zip(labels, predictions):
        if abs(l - p) < 1e-5:
            loss = loss + 1
    loss = loss / len(labels)

    return loss


# def cost(weights, bias, features, labels):
#     predictions = [variational_classifier(weights, bias, f) for f in features]
#     return square_loss(labels, predictions)
def cost(weights, features, labels):
    predictions = [variational_classifier(weights, f) for f in features]
    return square_loss(labels, predictions)


# event log in den handler laden
data_handler = EventLogHandler()
data_handler.load(str(Path(['resources', '20220105_test_tokens.xes'])))
data_handler.process_definition = MiningUtils.process_definition_from_event_log(data_handler.dataset)
performance_spectrum = MiningUtils.performance_spectrum_from_event_log(data_handler.dataset)

data_handler.time_series = MiningUtils.time_series_from_event_log(data_handler.dataset)

builder = EventLogEncodingBuilder() \
    .add(IndexBasedEncoder(window=2, normalization=True))

X, Y_temp = builder.build(data_handler)
X[X == 0.0] = 0.0001

Y = Y_temp / NO_WIRES**2

# TODO: number of wires must be determined automatically by the number of wires

np.random.seed(0)
num_data = len(Y)
num_train = int(0.75 * num_data)
index = np.random.permutation(range(num_data))
X_train, X_val, Y_train, Y_val = train_test_split(X, Y, test_size=0.2, random_state=3)

num_train = len(X_train)

weights_init = 0.01 * np.random.randn(NO_LAYERS, NO_WIRES, 3, requires_grad=True)
bias_init = np.zeros(NO_WIRES**2, requires_grad=True)

opt = AdamOptimizer()
batch_size = 5

# train the variational classifier
weights = weights_init
bias = bias_init

for it in range(60):
    # Update the weights by one optimizer step
    batch_index = np.random.randint(0, num_train, (batch_size,))
    feats_train_batch = X_train[batch_index]
    Y_train_batch = Y_train[batch_index]
    # https://discuss.pennylane.ai/t/variational-classifiers-and-qngoptimizer/524/11
    # TODO: Optimizer might not work because of zeros in x?
    print(feats_train_batch)
    # TODO: Optimizer might not be able to deal with bias?
    #weights, bias, _, _ = opt.step(cost, weights, bias, feats_train_batch, Y_train_batch)
    weights, _, _ = opt.step(cost, weights, feats_train_batch, Y_train_batch)

    # Compute predictions on train and validation set
    # TODO: it takes the argmax as the result of the prediction
    predictions_train = [np.argmax(variational_classifier(weights, bias, f)) for f in X_train]
    predictions_val = [np.argmax(variational_classifier(weights, bias, f)) for f in X_val]

    # Compute accuracy on train and validation set
    acc_train = accuracy(Y_train, predictions_train)
    acc_val = accuracy(Y_val, predictions_val)

    print(
        "Iter: {:5d} | Cost: {:0.7f} | Acc train: {:0.7f} | Acc validation: {:0.7f} "
        "".format(it + 1, cost(weights, bias, X, Y), acc_train, acc_val)
    )
