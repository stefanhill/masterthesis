from typing import TYPE_CHECKING

from sklearn.base import BaseEstimator

from data.handler.EventLogHandler import EventLogHandler
from ml import AbstractPrediction
from ml.AbstractClassifier import AbstractClassifier
from ml.AbstractMetric import AbstractMetric
from ml.metrics.VectorMetric import VectorMetric
from utils.Decorators import timer, memory
from utils.Enums import PredictionType

if TYPE_CHECKING:
    from ml.encoding.BuilderConfiguration import BuilderConfiguration


class SKWrapper(AbstractClassifier):

    def __init__(self, model: 'BaseEstimator' = None, **kwargs):
        from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
        from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder

        super().__init__(**kwargs)
        self.model: 'BaseEstimator' = model

        self.prediction_type = PredictionType.ACTIVITY
        if self.encoder is None:
            self.encoder = EventLogEncodingBuilder() \
                .add(IndexBasedEncoder(window=4))

    @timer
    @memory
    def fit(self, train_handler: 'EventLogHandler', val_handler: 'EventLogHandler' = None):
        x_train, y_train = self.encoder.build(train_handler)
        self.model.fit(x_train, y_train)

    @timer
    @memory
    def predict(self, predict_handler: 'EventLogHandler',
                builder_configuration: 'BuilderConfiguration' = None) -> 'AbstractPrediction':
        x_predict, _ = self.encoder.build(predict_handler, builder_configuration=builder_configuration, predict=True)
        return self.model.predict(x_predict)

    def evaluate(self, val_handler: 'EventLogHandler') -> 'AbstractMetric':
        x_val, y_val = self.encoder.build(val_handler)
        return VectorMetric(y_val, self.model.predict(x_val))
