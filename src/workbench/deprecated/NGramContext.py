import typing

from data.dataset.EndEvent import EndEvent
from data.handler.EventLogHandler import EventLogHandler
from ml import AbstractPrediction
from ml.AbstractClassifier import AbstractClassifier
from ml.AbstractMetric import AbstractMetric
from utils.Decorators import timer, memory
from workbench.deprecated.deprecated.NGramEncoder import NGramEncoder


class NGramContext(AbstractClassifier):

    def __init__(self, n: int = 2, **kwargs):
        super().__init__(**kwargs)
        self._n = n
        self.model: typing.Dict[int, typing.Dict[str, typing.Dict[str, float]]] = {}
        if self.encoder is None:
            self.encoder = NGramEncoder(event_features=['concept:name'], target='concept:name')

    @property
    def encoder(self) -> 'NGramEncoder':
        return self._encoder

    @encoder.setter
    def encoder(self, value: 'NGramEncoder'):
        self._encoder = value

    @timer
    @memory
    def fit(self, data: 'EventLogHandler', evaluation_data: 'EventLogHandler' = None):

        self.model = {}

        for n in range(1, self._n + 1):

            self.model[n] = {}
            for trace in data.dataset.traces:

                i = 0
                while i < len(trace) - n:
                    prefix, label = self.encoder.encode(trace, trace.events[i:i+n+1])
                    if prefix not in self.model[n].keys():
                        self.model[n][prefix] = {}
                    if label not in self.model[n][prefix].keys():
                        self.model[n][prefix][label] = 0
                    self.model[n][prefix][label] += 1

                    i += 1

            for prefix in self.model[n].keys():
                total = sum(self.model[n][prefix].values())
                if total > 0:
                    for label in self.model[n][prefix].keys():
                        self.model[n][prefix][label] /= total

    @timer
    @memory
    def predict(self, data: 'EventLogHandler') -> 'AbstractPrediction':
        prediction = {}

        for i, trace in enumerate(data.dataset.traces):

            prediction[i] = {}
            for n in range(1, self._n + 1):
                prefix, _ = self.encoder.encode(trace, trace.events[-n:], include_target=False)
                if n < len(trace) and not isinstance(trace.events[-1], EndEvent):
                    if prefix in self.model[n].keys():
                        prediction[i][prefix] = self.model[n][prefix]
                    else:
                        print(prefix)

        return prediction

    def evaluate(self, data: 'EventLogHandler') -> 'AbstractMetric':
        pass

