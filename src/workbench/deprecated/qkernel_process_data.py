import logging

import jax
import numpy as np
import pennylane as qml
from pennylane import numpy as np

from data.handler.EventLogHandler import EventLogHandler
from data.mining.MiningUtils import MiningUtils
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)

# event log in den handler laden
data_handler = EventLogHandler()
data_handler.load(str(Path(['resources', '20220105_test_tokens.xes'])))
data_handler.process_definition = MiningUtils.process_definition_from_event_log(data_handler.dataset)
performance_spectrum = MiningUtils.performance_spectrum_from_event_log(data_handler.dataset)

data_handler.time_series = MiningUtils.time_series_from_event_log(data_handler.dataset)


builder = EventLogEncodingBuilder() \
    .add(IndexBasedEncoder(window=2, normalization=True))

X, Y = builder.build(data_handler)

# 0: ──H──RZ(0)──RY(3.09)────╭C───────────────────────────────────────────────────╭RZ(5.03)──H─────────RZ(1)─────RY(0.408)──╭C────────────────────────────────────────────────────╭RZ(1.49)──H─────────RZ(0)──────RY(5.14)───╭C─────────────────────────────────────────────────────╭RZ(4.88)──H─────────RZ(1)─────RY(6.12)──╭C──────────────────────────────────────────────────────╭RZ(1.86)──H─────────RZ(0)─────RY(0.181)──╭C──────────────────────────────────────────────────────╭RZ(5.22)──H─────────RZ(1)─────RY(3.89)──╭C──────────────────────────────────────────╭RZ(0.633)──╭RZ(-0.633)──────────────────────────────────────╭C──────────RY(-3.89)───RZ(-1)──H──╭RZ(-5.22)────────────────────────────────────────╭C──────────RY(-0.181)──RZ(0)───H──╭RZ(-1.86)────────────────────────────────────────╭C──────────RY(-6.12)──RZ(-1)──H──╭RZ(-4.88)────────────────────────────────────────╭C──────────RY(-5.14)──RZ(0)───H──╭RZ(-1.49)───────────────────────────────────────╭C──────────RY(-0.408)──RZ(-1)──H──╭RZ(-5.03)───────────────────────────────────────╭C──────────RY(-3.09)────RZ(0)───H──╭┤ Probs
# 1: ──H──RZ(1)──RY(0.0317)──╰RZ(2.07)──╭C─────────H─────────RZ(0)─────RY(0.104)──│─────────────────────────────────────────╰RZ(3.01)──╭C───────────H──────────RZ(1)─────RY(2.4)──│──────────────────────────────────────────╰RZ(4.75)──╭C──────────H───────────RZ(0)─────RY(2.38)──│────────────────────────────────────────╰RZ(4.79)──╭C───────────H──────────RZ(1)──────RY(3.84)──│─────────────────────────────────────────╰RZ(4.27)──╭C─────────H───────────RZ(0)──────RY(0.747)──│────────────────────────────────────────╰RZ(3.19)──╭C───────────────────────────────│───────────│───────────────────────────────────╭C───────────╰RZ(-3.19)──RY(-0.747)──RZ(0)───H──│─────────────────────────────────────╭C──────────╰RZ(-4.27)──RY(-3.84)───RZ(-1)──H──│────────────────────────────────────╭C───────────╰RZ(-4.79)──RY(-2.38)──RZ(0)───H──│────────────────────────────────────╭C───────────╰RZ(-4.75)──RY(-2.4)───RZ(-1)──H──│───────────────────────────────────╭C───────────╰RZ(-3.01)──RY(-0.104)──RZ(0)───H──│────────────────────────────────────╭C──────────╰RZ(-2.07)──RY(-0.0317)──RZ(-1)──H──├┤ Probs
# 2: ──H──RZ(0)──RY(3.29)───────────────╰RZ(5.4)──╭C─────────H─────────RZ(1)──────│──────────RY(5.01)──────────────────────────────────╰RZ(0.788)──╭C──────────H─────────RZ(0)────│──────────RY(4.43)───────────────────────────────────╰RZ(4.58)──╭C───────────H─────────RZ(1)─────│──────────RY(1.18)─────────────────────────────────╰RZ(0.888)──╭C──────────H──────────RZ(0)─────│──────────RY(3.73)──────────────────────────────────╰RZ(2.8)──╭C───────────H──────────RZ(1)──────│──────────RY(1.54)─────────────────────────────────╰RZ(0.974)──╭C───────────────────│───────────│───────────────────────╭C──────────╰RZ(-0.974)───RY(-1.54)──RZ(-1)──────H──────────│────────────────────────╭C───────────╰RZ(-2.8)────RY(-3.73)──RZ(0)───────H──────────│────────────────────────╭C──────────╰RZ(-0.888)───RY(-1.18)──RZ(-1)─────H──────────│───────────────────────╭C───────────╰RZ(-4.58)────RY(-4.43)──RZ(0)──────H──────────│───────────────────────╭C──────────╰RZ(-0.788)───RY(-5.01)──RZ(-1)──────H──────────│───────────────────────╭C───────────╰RZ(-5.4)────RY(-3.29)──RZ(0)────────H──────────├┤ Probs
# 3: ──H──RZ(1)──RY(5.14)─────────────────────────╰RZ(1.8)──╭C─────────H──────────│──────────RZ(0)─────RY(4.82)────────────────────────────────────╰RZ(3.04)──╭C─────────H────────│──────────RZ(1)─────RY(0.389)───────────────────────────────────╰RZ(0.556)──╭C─────────H─────────│──────────RZ(0)─────RY(4.49)───────────────────────────────────╰RZ(3.48)──╭C──────────H─────────│──────────RZ(1)─────RY(3.75)──────────────────────────────────╰RZ(0.175)──╭C──────────H──────────│──────────RZ(0)─────RY(2.48)───────────────────────────────────╰RZ(5.54)──╭C────────│───────────│────────────╭C─────────╰RZ(-5.54)───RY(-2.48)────RZ(0)──────H──────────────────────│───────────╭C───────────╰RZ(-0.175)───RY(-3.75)───RZ(-1)─────H──────────────────────│───────────╭C───────────╰RZ(-3.48)───RY(-4.49)────RZ(0)──────H─────────────────────│───────────╭C──────────╰RZ(-0.556)───RY(-0.389)───RZ(-1)─────H─────────────────────│───────────╭C──────────╰RZ(-3.04)───RY(-4.82)────RZ(0)──────H──────────────────────│───────────╭C──────────╰RZ(-1.8)─────RY(-5.14)───RZ(-1)─────H───────────────────────├┤ Probs
# 4: ──H──RZ(0)──RY(0.446)──────────────────────────────────╰RZ(5.76)─────────────╰C─────────H─────────RZ(1)─────RY(4.61)─────────────────────────────────────╰RZ(5.08)───────────╰C─────────H─────────RZ(0)──────RY(0.519)────────────────────────────────────╰RZ(4.21)────────────╰C─────────H─────────RZ(1)─────RY(3.58)────────────────────────────────────╰RZ(0.907)────────────╰C─────────H─────────RZ(0)─────RY(3.02)────────────────────────────────────╰RZ(0.876)─────────────╰C─────────H─────────RZ(1)─────RY(3.28)────────────────────────────────────╰RZ(1.2)──╰C──────────╰C───────────╰RZ(-1.2)───RY(-3.28)───RZ(-1)───────H─────────────────────────────────╰C──────────╰RZ(-0.876)───RY(-3.02)────RZ(0)───────H─────────────────────────────────╰C──────────╰RZ(-0.907)───RY(-3.58)───RZ(-1)───────H────────────────────────────────╰C──────────╰RZ(-4.21)───RY(-0.519)───RZ(0)────────H────────────────────────────────╰C──────────╰RZ(-5.08)───RY(-4.61)───RZ(-1)───────H─────────────────────────────────╰C──────────╰RZ(-5.76)───RY(-0.446)───RZ(0)───────H──────────────────────────────────╰┤ Probs
# https://pennylane.ai/qml/demos/tutorial_variational_classifier.html andere ansatz

# TODO: this is a layer of the kernel (Havlicek calls it feature map)
def layer(x, params, wires, i0=0, inc=1):
    """Building block of the embedding ansatz"""
    i = i0
    for j, wire in enumerate(wires):
        # TODO: for each iteration
        # TODO: add one Hadamard
        qml.Hadamard(wires=[wire])
        # TODO: one z rotation, modulo ensures that all params will occur exactly once
        # x is encoded in the rz layer
        qml.RZ(x[i % len(x)], wires=[wire])
        i += inc
        # TODO: one ry rotation per layer
        qml.RY(params[0, j], wires=[wire])

    # TODO: https://pennylane.readthedocs.io/en/stable/code/api/pennylane.broadcast.html
    # TODO: connects every wire with every other wire
    qml.broadcast(unitary=qml.CRZ, pattern="ring", wires=wires, parameters=params[1])


def ansatz(x, params, wires):
    """The embedding ansatz"""
    # TODO: equivalent to the ansatz of feature maps in qiskit
    # enumerate(params) here means number of layers
    for j, layer_params in enumerate(params):
        # create a layer with vector on layer arguments in params
        layer(x, layer_params, wires, i0=j * len(wires))


# TODO: adjoint means just the kinda "transpose" of the ansatz
adjoint_ansatz = qml.adjoint(ansatz)


def random_params(num_wires, num_layers):
    """Generate random variational parameters in the shape for the ansatz."""
    # TODO: uniformly sampled parameters in the shape of the ansatz
    # TODO: shape is the number of layers, (why 2? -> because ry params and crz params) and the number of wires
    return np.random.uniform(0, 2 * np.pi, (num_layers, 2, num_wires), requires_grad=True)


# TODO: initialize device with wires=len(feature_vector)
# TODO: jax backend
dev = qml.device("default.qubit", wires=5, shots=None)
# TODO: this variable is just a list of all wires, -> useful when it comes to adding them in a function
wires = dev.wires.tolist()


# TODO: ansatz added to the qml device here
@qml.qnode(dev, interface='jax')
def kernel_circuit(x1, x2, params):
    # iterate over params and create circuit once
    ansatz(x1, params, wires=wires)
    # the ansatz encodes two data points <x1|x2> so we can
    adjoint_ansatz(x2, params, wires=wires)
    return qml.probs(wires=wires)
    # TODO: only the number of wires has to be different
    # TODO: generify such that we can try different kernels (here h, rz, ry)
# which kernels are there and what might be suited for our dataset


def kernel(x1, x2, params):
    # returns only the first element of the prediction
    return jax.jit(kernel_circuit(x1, x2, params)[0])
    #return kernel_circuit(x1, x2, params)[0]


# TODO: initial parameters are sampled for the circuit
init_params = random_params(num_wires=5, num_layers=6)

kernel_value = kernel(X[0], X[1], init_params)
# TODO: refers to the "distance"
print(f"The kernel value between the first and second datapoint is {kernel_value:.3f}")


# TODO: here we initialize a kernel with random values to be used for prediction
init_kernel = lambda x1, x2: kernel(x1, x2, init_params)
# takes a function init_kernel that maps datapoints from X to the respective kernel value
#K_init = qml.kernels.square_kernel_matrix(X, init_kernel, assume_normalized_kernel=True)

# [[1.    0.002 0.038 0.033 0.019]
#  [0.002 1.    0.152 0.046 0.056]
#  [0.038 0.152 1.    0.005 0.022]
#  [0.033 0.046 0.005 1.    0.032]
#  [0.019 0.056 0.022 0.032 1.   ]]

# the kernel matrix is symmetric
# TODO: this kernel matrix is for made for 5 data points, how can we scale it?
#with np.printoptions(precision=3, suppress=True):
#    print(K_init)

drawer = qml.draw(kernel_circuit)
print(drawer(X[0], X[0], init_params))

from sklearn.svm import SVC

# TODO: qml.kernels.kernel_matrix returns the value for the kernel
svm = SVC(kernel=lambda X1, X2: qml.kernels.kernel_matrix(X1, X2, init_kernel)).fit(X, Y)


def accuracy(classifier, X, Y_target):
    return 1 - np.count_nonzero(classifier.predict(X) - Y_target) / len(Y_target)


accuracy_init = accuracy(svm, X, Y)
print(f"The accuracy of the kernel with random parameters is {accuracy_init:.3f}")
