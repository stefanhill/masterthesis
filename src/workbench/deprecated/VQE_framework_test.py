import logging

from data.handler.EventLogHandler import EventLogHandler
from ml.classifier.VQC import VQC
from ml.classifier.quantum.QuantumEmbedding import QuantumEmbedding
from ml.classifier.quantum.QuantumLayer import QuantumLayer
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)

data_handler = EventLogHandler()
data_handler.load(str(Path(['resources', '20220105_test_tokens.xes'])), loader_kwargs={'wrap_trace': False})

# TODO: currently, number of instances is cut off inside qke.fit(), implement methods for train test split and sampling


encoder = EventLogEncodingBuilder() \
    .add(IndexBasedEncoder(window=4, normalization=True))

vqc = VQC(n_layers=3, layer=QuantumLayer.default_variational, state_preparation=QuantumEmbedding.angle,
          encoder=encoder)
vqc.fit(data_handler)

metric = vqc.evaluate(data_handler)
