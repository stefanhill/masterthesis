import pm4py

from data.processing.filter.Filters import Filters
from data.processing.pm4py.ManualRemoveEvents import ManualRemoveEvents
from data.processing.pm4py.ReduceEndEvents import ReduceEndEvents
from data.processing.pm4py.RemoveEventsByLifecycle import RemoveEventsByLifecycle
from data.processing.pm4py.RemoveEventsByName import RemoveEventsByName
from data.processing.pm4py.RenameEvents import RenameEvents
from utils.Utils import Utils

event_log = pm4py.read_xes('C:\\git\\spa-ws2122\\resources\\BPI_Challenge_2012.xes')

name_mapping = {
    'W_Afhandelen leads': 'W_Fixing incoming lead',
    'W_Completeren aanvraag': 'W_Filling in information for the application',
    'W_Valideren aanvraag': 'W_Assessing the application',
    'W_Nabellen offertes': 'W_Calling after sent offers',
    'W_Nabellen incomplete dossiers': 'W_Calling to add missing information to the application'
}

redundant_events = ['A_PARTLYSUBMITTED', 'O_SELECTED', 'O_CREATED', 'O_ACCEPTED', 'A_REGISTERED', 'A_ACTIVATED',
                    'O_DECLINED']  # @Ali: add other ones if necessary

end_events = ['A_DECLINED', 'A_APPROVED', 'A_CANCELLED', 'O_CANCELLED']

event_log = Utils.apply_preprocessing_chain(event_log, [
    RemoveEventsByName(names=redundant_events),
    ManualRemoveEvents(),
    RemoveEventsByLifecycle(lifecycles=['COMPLETE'], is_manual=True),
    ReduceEndEvents(keep=end_events),
    RenameEvents(name_mapping=name_mapping),
])
# this one removes all cases do end with a W event after preprocessing i.e. unfinished cases
event_log = pm4py.filter_log(Filters.end_events, event_log)

# @Ali: use this to save the log
pm4py.write_xes(event_log, 'C:\\git\\spa-ws2122\\resources\\BPI_Challenge_2012_prep_analyse_20211212_filtered.xes')
