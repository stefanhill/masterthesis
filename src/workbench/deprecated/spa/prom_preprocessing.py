import pm4py

from data.processing.filter.Filters import Filters
from data.processing.pm4py.ManualRemoveEvents import ManualRemoveEvents
from data.processing.pm4py.ReduceEndEvents import ReduceEndEvents
from data.processing.pm4py.ReduceLoops import ReduceLoops
from data.processing.pm4py.RemoveEventsByLifecycle import RemoveEventsByLifecycle
from data.processing.pm4py.RemoveEventsByName import RemoveEventsByName
from data.processing.pm4py.RenameEvents import RenameEvents
from utils.Utils import Utils

event_log = pm4py.read_xes('C:\\git\\spa-ws2122\\resources\\BPI_Challenge_2012.xes')

name_mapping = {
    'W_Afhandelen leads': 'W_Fixing_incoming_lead',
    'W_Completeren aanvraag': 'W_Filling_in_information_for_the_application',
    'W_Valideren aanvraag': 'W_Assessing_the_application',
    'W_Nabellen offertes': 'W_Calling_after_sent_offers',
    'W_Nabellen incomplete dossiers': 'W_Calling_to_add_missing_information_to_the_application',
}

redundant_events = ['A_PARTLYSUBMITTED', 'O_SELECTED', 'O_CREATED', 'O_ACCEPTED', 'A_REGISTERED', 'A_ACTIVATED',
                    'O_DECLINED']  # @Ali: add other ones if necessary

end_events = ['A_DECLINED', 'A_APPROVED', 'A_CANCELLED']

event_log = Utils.apply_preprocessing_chain(event_log, [
    RemoveEventsByName(names=redundant_events),
    ManualRemoveEvents(),
    RemoveEventsByLifecycle(lifecycles=['COMPLETE', 'SCHEDULE'], is_manual=True),
    ReduceLoops(),
    ReduceEndEvents(keep=end_events),
    RenameEvents(name_mapping=name_mapping),
])

event_log = pm4py.filter_log(Filters.end_events, event_log)


# @Ali: use this to save the log
pm4py.write_xes(event_log, 'C:\\git\\spa-ws2122\\resources\\BPI_Challenge_2012_prep_analyse_20211215_filtered_loop.xes')
