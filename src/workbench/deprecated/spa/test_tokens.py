from data.handler.EventLogHandler import EventLogHandler
from workbench.deprecated.deprecated import Tokenizer

data_handler = EventLogHandler()
data_handler.load('C:\\git\\spa-ws2122\\resources\\20220105_test_tokens.xes')

part_trace = data_handler.dataset[0]

tokens = Tokenizer(token_size=2, start_index=-1).tokenize(data_handler.dataset.traces[0])