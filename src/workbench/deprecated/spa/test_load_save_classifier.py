import os.path

from data.handler.EventLogHandler import EventLogHandler
from ml.AbstractClassifier import AbstractClassifier
from utils.Utils import Utils
from workbench.deprecated.NGramContext import NGramContext

CLASSIFIER_NAME = 'testNgram'

CLASSIFIER_PATH = os.path.join('C:\\git\\spa-ws2122\\appdata\\classifier', f'{CLASSIFIER_NAME}.p')

TRAINING_DATA_PATH = 'C:\\git\\spa-ws2122\\appdata\\temp\\training.xes'

data_handler = EventLogHandler()
data_handler.load(TRAINING_DATA_PATH)

classifier: 'AbstractClassifier'
if f'{CLASSIFIER_NAME}.p' in 'C:\\git\\spa-ws2122\\appdata\\classifier':
    classifier = Utils.load_classifier(CLASSIFIER_PATH)
else:
    classifier = NGramContext()

classifier.fit(data_handler)
classifier.save(CLASSIFIER_PATH)
