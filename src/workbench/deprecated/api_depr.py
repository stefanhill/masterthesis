import os

from flask import Flask, redirect, flash
from flask import request, jsonify, make_response
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = 'C:\\git\\spa-ws2122\\resources\\uploads'
ALLOWED_EXTENSIONS = {'txt', 'xes'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/file', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            resp = {
                'upload': True,
            }
            return make_response(jsonify(resp), 200)


if __name__ == '__main__':
    app.run(debug=True)
