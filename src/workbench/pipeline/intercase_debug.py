import copy
import logging

import pandas as pd

from data.handler.EventLogHandler import EventLogHandler
from utils.Path import Path

logging.basicConfig(level=logging.WARNING)

handler = EventLogHandler()
handler.load(str(Path(['resources', 'test_logs', 'bpi2011_prep.xes'])))

import datetime

from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intercase.AverageDelay import AverageDelay
from ml.encoding.intercase.FrequentPreviousActivity import FrequentPreviousActivity
from ml.encoding.intercase.FutureBatchingBehaviour import FutureBatchingBehaviour
from ml.encoding.intercase.NoPeerCases import NoPeerCases
from ml.encoding.intercase.PeerActivityCount import PeerActivityCount
from ml.encoding.intercase.ResourceCount import ResourceCount
from ml.encoding.intercase.TopBusyResource import TopBusyResource
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from ml.classifier.NGram import NGram

MEDIAN_CASE_TIME = 27.1

timedeltas = [datetime.timedelta(weeks=0.1 * MEDIAN_CASE_TIME),
              datetime.timedelta(weeks=0.15 * MEDIAN_CASE_TIME),
              datetime.timedelta(weeks=0.25 * MEDIAN_CASE_TIME),
              datetime.timedelta(weeks=0.5 * MEDIAN_CASE_TIME),
              datetime.timedelta(weeks=0.75 * MEDIAN_CASE_TIME)]

for timedelta in timedeltas:
    handler_ = copy.deepcopy(handler)

    handler_.mine_batching_patterns(min_batch_size=10, max_delay=0.1 * timedelta)

    ngram = NGram(max_classes=len(handler_.process_definition))
    ngram_builder = EventLogEncodingBuilder() \
        .add(IndexBasedEncoder(window=4))
    X_ngram, y_ngram = ngram_builder.build(handler_)

    ngram.fit(X_ngram, y_ngram)

    builder = EventLogEncodingBuilder() \
        .add(IndexBasedEncoder(window=4)) \
        .add(NoPeerCases(timeframe=timedelta)) \
        .add(PeerActivityCount(timeframe=timedelta)) \
        .add(ResourceCount(timeframe=timedelta)) \
        .add(AverageDelay(timeframe=timedelta)) \
        .add(FrequentPreviousActivity(timeframe=timedelta, no_activities=2, normalization=True)) \
        .add(TopBusyResource(timeframe=timedelta, no_resources=2, normalization=True)) \
        .add(FutureBatchingBehaviour(segment_classifier=ngram, segment_encoding_builder=ngram_builder, include_average_times=False)) \

    X, y = builder.build(handler_)

    df_values = {}

    for i_ in range(4):
        df_values[f'index_based{i_}'] = X[:, i_]
    df_values['peer_cases'] = X[:, 4]
    df_values['peer_act'] = X[:, 5]
    df_values['res_count'] = X[:, 6]
    df_values['avg_delay'] = X[:, 7]
    df_values['freq_act'] = X[:, 8]
    df_values['top_res'] = X[:, 9]
    df_values['batch'] = X[:, 10]

    df = pd.DataFrame(df_values)
    print(df.head())