import datetime
import logging

from data.handler.EventLogHandler import EventLogHandler
from ml.classifier.NGram import NGram
from ml.classifier.VQC import VQC
from ml.classifier.quantum.QuantumEmbedding import QuantumEmbedding
from ml.classifier.quantum.QuantumLayer import QuantumLayer
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)

timedelta = datetime.timedelta(days=1)

train_handler = EventLogHandler()
train_handler.load(str(Path(['resources', '20220613_test_rtf.xes'])))

val_handler = EventLogHandler()
val_handler.load(str(Path(['resources', '20220613_test_rtf.xes'])))

# Ensure the mapping is the same for train and validation handler
val_handler.process_definition = train_handler.process_definition

train_handler.mine_batching_patterns(min_batch_size=5, max_delay=datetime.timedelta(hours=3))
val_handler.mine_batching_patterns(min_batch_size=5, max_delay=datetime.timedelta(hours=3))

ngram = NGram(n=3)
ngram.fit(train_handler)

# builder = EventLogEncodingBuilder() \
#     .add(AggregationEncoder()) \
#     .add(IndexBasedEncoder()) \
#     .add(NoPeerCases(timeframe=timedelta)) \
#     .add(PeerActivityCount(timeframe=timedelta)) \
#     .add(FrequentPreviousActivity(timeframe=timedelta, no_activities=2)) \
#     .add(ResourceCount(timeframe=timedelta)) \
#     .add(TopBusyResource(timeframe=timedelta, no_resources=2)) \
#     .add(FutureBatchingBehaviour(segment_classifier=ngram, include_average_times=False)) \
#     .add(AverageDelay(timeframe=timedelta))
#
# x, y = builder.build(train_handler)
# x_val, y_val = builder.build(val_handler)

intra_case_encoder = EventLogEncodingBuilder() \
    .add(IndexBasedEncoder(window=4, normalization=True))

accuracies = []
classifiers = [
    #NGram(n=4, encoder=intra_case_encoder),
    #SKWrapper(model=RandomForestClassifier(max_depth=2, random_state=0), encoder=intra_case_encoder),
    #SKWrapper(model=XGBClassifier(), encoder=intra_case_encoder),
    #QKE(n_layers=2, kernel=QuantumEmbedding.angle, encoder=intra_case_encoder),
    VQC(n_layers=3, layer=QuantumLayer.default_variational, state_preparation=QuantumEmbedding.zz,
        encoder=intra_case_encoder)
]

for clf in classifiers:
    clf.fit(train_handler)
    accuracies.append(clf.evaluate(val_handler))
