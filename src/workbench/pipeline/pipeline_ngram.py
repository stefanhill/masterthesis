import datetime
import logging

from data.handler.EventLogHandler import EventLogHandler
from ml.classifier.NGram import NGram
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)

timedelta = datetime.timedelta(days=1)

train_handler = EventLogHandler()
train_handler.load(str(Path(['resources', 'test_logs', 'bpi2011_train.xes'])))

val_handler = EventLogHandler()
val_handler.load(str(Path(['resources', 'test_logs', 'bpi2011_validation.xes'])))

# Ensure the mapping is the same for train and validation handler
val_handler.process_definition = train_handler.process_definition

#train_handler.mine_batching_patterns(min_batch_size=5, max_delay=datetime.timedelta(hours=3))
#val_handler.mine_batching_patterns(min_batch_size=5, max_delay=datetime.timedelta(hours=3))

ngram = NGram(n=3)
ngram.fit(train_handler)

intra_case_encoder = EventLogEncodingBuilder() \
    .add(IndexBasedEncoder(window=4, normalization=True)) \

acc = ngram.evaluate(val_handler).accuracy()
