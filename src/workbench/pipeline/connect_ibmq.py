import pennylane as qml
from pennylane import numpy as qnp

# from qiskit import IBMQ
# provider = IBMQ.enable_account('8dc22058e9293e2fcbd6bb85e45735e8b621bb29cd06326e13f2657abff2514b7f24d0958087b377b8d0d5b8155c5cf4755cd01dee98cd6818c7cd14d3fc81bc')

dev = qml.device('qiskit.ibmq', wires=2, backend='ibmq_qasm_simulator',
                 ibmqx_token='8dc22058e9293e2fcbd6bb85e45735e8b621bb29cd06326e13f2657abff2514b7f24d0958087b377b8d0d5b8155c5cf4755cd01dee98cd6818c7cd14d3fc81bc',
                 hub='ibm-q-research-2', group='uni-koblenz-land-1', project='main')

@qml.qnode(dev)
def circuit(x):
    qml.Hadamard(wires=0)
    qml.Hadamard(wires=1)
    qml.RZ(x[0], wires=0)
    qml.RZ(x[1], wires=1)
    qml.CNOT(wires=[0, 1])
    return qml.expval(qml.PauliY(0))

X = qnp.array([[0.5, 0.5],
               [0.25, 0.1],
               [0.5, 0.1]])

y = [circuit(x_) for x_ in X]

print(y)

