import abc
import typing

from data.AbstractDataInstance import AbstractDataInstance


class AbstractEncoder(abc.ABC):

    @abc.abstractmethod
    def encode(self, *data: typing.List['AbstractDataInstance']) -> typing.List[typing.Tuple[typing.Any, typing.Any]]:
        pass
