import logging

try:
    from jax.config import config

    config.update("jax_enable_x64", True)
    import jax
    from jax import numpy as jnp
except ImportError as e:
    logging.warning('Jaxlib not found during init of metric functions. Cannot use jax_square_loss!')

from pennylane import numpy as pnp


class MetricFunctions:

    @staticmethod
    def square_loss(labels, predictions):
        loss = 0
        for l, p in zip(labels, predictions):
            loss = loss + pnp.dot((l - p), (l - p).T)

        loss = loss / len(labels)
        return loss

    @staticmethod
    def jax_square_loss(labels, predictions):
        loss = 0
        for l, p in zip(labels, predictions):
            loss = loss + jnp.dot((l - p), (l - p).T)

        loss = loss / len(labels)
        return loss
