import abc
import typing

import numpy as np

from data.processing.AbstractProcessor import AbstractProcessor


class FeatureLabelProcessor(AbstractProcessor, abc.ABC):

    @abc.abstractmethod
    def process(self, data: typing.Tuple[np.ndarray, np.ndarray]) -> typing.Tuple[np.ndarray, np.ndarray]:
        pass
