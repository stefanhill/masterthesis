import typing


class BuilderConfiguration:
    """
    Builder configuration object that can be passed to an encoding builder if one does not want
    to build the complete data handler, but only specified parts
    """

    def __init__(self,
                 trace_identifier: typing.Union[typing.List[typing.Any], typing.Any],
                 event_identifier: typing.Union[typing.List[typing.Any], typing.Any],):
        """
        Initialisation of the builder configuration with lists of trace and event identifiers
        List must have equal length

        Args:
            trace_identifier: a list of trace identifiers
            event_identifier: a list of event identifiers
        """
        if isinstance(trace_identifier, list):
            self._trace_identifier = trace_identifier
        else:
            self._trace_identifier = [trace_identifier]

        if isinstance(event_identifier, list):
            self._event_identifier = event_identifier
        else:
            self._event_identifier = [event_identifier]

        assert len(self._trace_identifier) == len(self._event_identifier)

    @property
    def trace_identifier(self):
        return self._trace_identifier

    @trace_identifier.setter
    def trace_identifier(self, value):
        self._trace_identifier = value

    @property
    def event_identifier(self):
        return self._event_identifier

    @event_identifier.setter
    def event_identifier(self, value):
        self._event_identifier = value