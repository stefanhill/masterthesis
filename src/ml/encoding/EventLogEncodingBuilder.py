import typing

import numpy as np

from data.dataset.Trace import Trace
from data.handler.EventLogHandler import EventLogHandler
from ml.EventLogEncoder import EventLogEncoder
from ml.encoding.BuilderConfiguration import BuilderConfiguration
from ml.encoding.FeatureLabelProcessor import FeatureLabelProcessor


class EventLogEncodingBuilder:

    def __init__(self):
        self._cache: typing.Dict[int, typing.Tuple[typing.Any, typing.Any]] = {}
        self._encoders: typing.List['EventLogEncoder'] = []
        self._post_processors: typing.List['FeatureLabelProcessor'] = []

    def add(self, encoder: 'EventLogEncoder') -> 'EventLogEncodingBuilder':
        """
        Adds an encoder to the list of encoders

        Args:
            encoder: event log encoder to be added

        Returns:
            reference to the object itself

        """
        self._encoders.append(encoder)
        return self

    def add_processor(self, processor: 'FeatureLabelProcessor') -> 'EventLogEncodingBuilder':
        self._post_processors.append(processor)
        return self

    # TODO: add label encoding function to the builder, currently only supports next activity predictions
    # TODO: find a way to reduce cognitive complexity
    def build(self, data: 'EventLogHandler', predict=False,
              overwrite_cache: bool = False,
              builder_configuration: 'BuilderConfiguration' = None) -> \
            typing.Tuple[typing.Any, typing.Any]:
        """Builds the X, y (feature, label) for a data handler by executing all encoders on all events

        In predict mode, labels are np.nan

        Args:
            data: EventLogHandler with to create features of
            predict: if predict mode True, labels will be np.nan
                    features will be created only for the last event in the trace
            overwrite_cache: If True builder is forced to rebuild and caches are overwritten
            builder_configuration: builds only ids from configuration object

        Returns: X, y

        """
        features, labels = np.array([]), np.array([])

        if not predict and not overwrite_cache:
            if id(data) in self._cache.keys() and self._cache[id(data)] is not None:
                return self._cache[id(data)]

        if builder_configuration is None:
            for trace in data.dataset.traces:
                i_start = len(trace.events) - 1 if predict else 0
                i_end = len(trace.events) if predict else len(trace.events) - 1
                for i in range(i_start, i_end):
                    feature, label = self._get_feature_label(data, trace, i, predict=predict)
                    features = np.append(features, feature)
                    labels = np.append(labels, label)
        else:
            for trace_id, event_id in zip(builder_configuration.trace_identifier,
                                          builder_configuration.event_identifier):
                feature, label = self._get_feature_label(data, data.dataset.get_trace_by_id(trace_id), event_id,
                                                         predict=predict)
                features = np.append(features, feature)
                labels = np.append(labels, label)

        assert len(features) % len(labels) == 0
        features = features.reshape((len(labels), int(len(features) / len(labels))))

        for processor in self._post_processors:
            features, labels = processor.process((features, labels))

        self._cache[id(data)] = (features, labels)
        return features, labels

    def _get_feature_label(self, data: 'EventLogHandler', trace: 'Trace', event_index: int, predict: bool = False):
        """
        Helper method to create the feature label pair for one event

        Args:
            data: data handler with event log
            trace: current trace id
            event_index: current event id
            predict: Bool to indicate whether to add label or not (if True no label added)

        Returns:
            Tuple of numpy arrays (feature, label)

        """
        event = trace.events[event_index]
        feature = np.array([])
        for encoder in self._encoders:
            # TODO: performance might be even better if window is only calculated once and passed to every encoder
            feature_ = encoder.encode(data, trace.identifier, event.identifier)
            feature = np.concatenate([feature, feature_])
        if predict:
            label = np.nan
        else:
            label = data.process_definition.activity_to_id[trace.events[event_index + 1].attributes['concept:name']]
        return feature, label

    def clear_cache(self):
        self._cache = None

    def to_cache(self, data):
        if isinstance(data, tuple):
            features, labels = data
            self._cache[id(data)] = (features, labels)
        elif isinstance(data, EventLogHandler):
            self.build(data, overwrite_cache=True)

    def from_cache(self, data_id):
        return self._cache[data_id]
