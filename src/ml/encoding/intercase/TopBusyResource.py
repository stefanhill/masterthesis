import numpy as np

from data.handler.EventLogHandler import EventLogHandler
from ml.encoding.InterCaseEncoder import InterCaseEncoder


class TopBusyResource(InterCaseEncoder):
    """
    Inter-case encoder to return top most busy resources in the reference window
    """

    def __init__(self, no_resources: int = 1,
                 normalization: bool = False, **kwargs):
        """
        Initialisation of the encoder

        Args:
            no_resources: Number of most frequent resources to be returned
            **kwargs:
        """
        super().__init__(**kwargs)
        self._no_resources = no_resources
        self._normalization = normalization

    def encode(self, data_handler: 'EventLogHandler', trace_identifier, event_identifier) -> np.ndarray:
        """
        Encodes the top busiest resources for a given data handler

        Args:
            data_handler: data handler containing event log and time series
            trace_identifier: reference trace id
            event_identifier: reference event id

        Returns:
            A numpy array with the top n busiest resources
            - 1 if there are not enough resources in the reference window

        """
        window = self.get_window(data_handler, trace_identifier, event_identifier)
        top_frequent = window['org:resource'].value_counts()[:self._no_resources].keys()
        feature = np.zeros(self._no_resources) - 1
        for i, resource in enumerate(top_frequent):
            feature[i] = data_handler.process_definition.resource_to_id[resource]
        # for amplitude normalization divide by the number of distinct events
        if self._normalization:
            # +1 because of nan events==-1 and to avoid division by zero
            return (feature + 1) / (len(data_handler.process_definition.resource_to_id.keys()) + 1)
        else:
            return feature
