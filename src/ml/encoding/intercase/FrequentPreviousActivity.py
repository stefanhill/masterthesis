import numpy as np

from data.handler.EventLogHandler import EventLogHandler
from ml.encoding.InterCaseEncoder import InterCaseEncoder


class FrequentPreviousActivity(InterCaseEncoder):
    """
    Inter-case encoder to return the top most frequent activities
    """

    def __init__(self, no_activities: int = 1,
                 normalization: bool = False, **kwargs):
        """
        Initialises the inter-case encoder

        Args:
            no_activities: count of top most activities to return in the feature vector
            **kwargs:
        """
        super().__init__(**kwargs)
        self._no_activities = no_activities
        self._normalization = normalization

    def encode(self, data_handler: 'EventLogHandler', trace_identifier, event_identifier) -> np.ndarray:
        """
        Encode the top most frequent activities in the reference window

        Args:
            data_handler: data handler with event log and time series
            trace_identifier: reference trace id
            event_identifier: reference event id

        Returns:
            A numpy array with the top most n frequent activities
            -1 if there are not enough activities inside the reference window

        """
        window = self.get_window(data_handler, trace_identifier, event_identifier)
        top_frequent = window['concept:name'].value_counts()[:self._no_activities].keys()
        feature = np.zeros(self._no_activities) - 1
        for i, activity in enumerate(top_frequent):
            feature[i] = data_handler.process_definition.activity_to_id[activity]
        # for amplitude normalization divide by the number of distinct events
        if self._normalization:
            # +1 because of nan events==-1 and to avoid division by zero
            return (feature + 1) / (len(data_handler.process_definition) + 1)
        else:
            return feature
