import typing

import numpy as np

from data.handler.EventLogHandler import EventLogHandler
from ml.MLUtils import MLUtils
from ml.encoding.InterCaseEncoder import InterCaseEncoder


class NoPeerCases(InterCaseEncoder):
    """
    Inter-case encoder to return the number of peer cases in the reference window
    """

    def __init__(self, normalization: typing.Tuple[float, float] = None, **kwargs):
        super().__init__(**kwargs)
        self._normalization = normalization

    def encode(self, data_handler: 'EventLogHandler', trace_identifier, event_identifier) -> np.ndarray:
        """
        Encodes the number of peer cases in the reference window

        Args:
            data_handler: data handler containing event log and time series
            trace_identifier: reference trace id
            event_identifier: reference event id

        Returns:
            A numpy array with one element, the number of peer cases in the reference window

        """
        window = self.get_window(data_handler, trace_identifier, event_identifier)
        feature = np.array([window['trace_identifier'].nunique()])
        if self._normalization is not None:
            return MLUtils.min_max_normalization(feature, *self._normalization)
        else:
            return feature
