import abc

import numpy as np

from data.handler.EventLogHandler import EventLogHandler
from ml.AbstractEncoder import AbstractEncoder


class EventLogEncoder(AbstractEncoder, abc.ABC):

    @abc.abstractmethod
    def encode(self, data_handler: 'EventLogHandler', trace_identifier, event_identifier) -> 'np.ndarray':
        pass
