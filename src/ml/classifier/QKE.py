import logging
import sys
import time

import numpy as np
from sklearn.base import ClassifierMixin, BaseEstimator

try:
    from jax.config import config

    config.update("jax_enable_x64", True)
    import jax
except ImportError as e:
    logging.warning('Jaxlib not found during init of QKE. Use pip install jaxlib to enable circuit compilation!')

import pennylane as qml
from pennylane import numpy as pnp
from sklearn.svm import SVC

from ml.classifier.quantum.QuantumEmbedding import QuantumEmbedding
from ml.metrics.VectorMetric import VectorMetric


class QKE(BaseEstimator, ClassifierMixin):
    def __init__(self, kernel=QuantumEmbedding.angle, n_layers: int = 2,
                 n_wires: int = 2, dev=None, use_jax: bool = False, debug: bool = False,
                 **kwargs):

        super().__init__(**kwargs)
        self.model = None
        self.debug = debug

        self.use_jax = use_jax
        self.interface = 'jax' if self.use_jax else None

        self.kernel = kernel
        self.n_layers = n_layers
        self.n_wires = n_wires

        self.dev = dev
        self.x_train = pnp.array([])

    def _kernel_matrix(self, A, B):
        """Compute the matrix whose entries are the kernel
           evaluated on pairwise data from sets A and B."""

        projector_ = pnp.zeros((2 ** self.n_wires, 2 ** self.n_wires))
        projector_[0, 0] = 1

        @qml.qnode(self.dev, interface=self.interface)
        def _kernel_embedding(x, y):
            self.kernel(x, n_layers=self.n_layers, n_wires=self.n_wires)
            adj_ansatz = qml.adjoint(self.kernel)
            adj_ansatz(y, n_layers=self.n_layers, n_wires=self.n_wires)
            return qml.expval(qml.Hermitian(projector_, wires=range(self.n_wires)))

        circuit_ = _kernel_embedding if not self.use_jax else jax.jit(_kernel_embedding)

        total_len = len(B) * len(A)
        counter = 0
        arr = pnp.empty(shape=(len(A), len(B)))
        start_time = time.time()
        for i, a in enumerate(A):
            for j, b in enumerate(B):
                arr[i, j] = circuit_(a, b)
                if self.debug:
                    time_diff = time.time() - start_time
                    sys.stdout.write(
                        f'\r{counter}/{total_len} : {(100 * counter / total_len):.3f}% : time {int(time_diff / 60)}min')
                    sys.stdout.flush()
                counter += 1
        sys.stdout.write('\n')
        return arr

    def fit(self, X: np.ndarray, y: np.ndarray) -> 'QKE':
        # If amplitude encoding, n_qubits = log_2(n_features)
        # self._n_wires = np.int64(np.ceil(np.log2(len(X[0])))) \
        #     if self.kernel == QuantumEmbedding.amplitude \
        #     else len(X[0])
        #
        # if self.kernel == QuantumEmbedding.amplitude:
        #     X = MLUtils.prep_features_for_amplitude_encoding(X)
        self.n_wires = len(X[0])

        # Automatically set device using determined number of wires
        if self.dev is None:
            self.dev = qml.device("default.qubit", wires=self.n_wires)

        self.x_train = X
        matrix_train = self._kernel_matrix(X, X)

        self.model = SVC(kernel='precomputed')
        self.model.fit(matrix_train, y)

        return self

    def predict(self, X: np.ndarray) -> np.ndarray:
        matrix_predict = self._kernel_matrix(X, self.x_train)
        prediction = self.model.predict(matrix_predict)

        return prediction

    def predict_proba(self, X: np.ndarray) -> np.ndarray:
        matrix_predict = self._kernel_matrix(X, self.x_train)
        prediction = self.model.predict_proba(matrix_predict)

        return prediction

    def score(self, X: np.ndarray, y: np.ndarray, sample_weight=None) -> float:

        matrix_val = self._kernel_matrix(X, self.x_train)
        prediction = self.model.predict(matrix_val)

        return VectorMetric(y, prediction).accuracy()
