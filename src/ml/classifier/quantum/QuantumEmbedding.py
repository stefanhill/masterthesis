import math

import pennylane as qml
from pennylane import AngleEmbedding, BasisEmbedding, IQPEmbedding, AmplitudeEmbedding


class QuantumEmbedding:
    """
    Source: https://github.com/davidfitzek/bachelor_thesis_qml/blob/main/QKE/QKE_PennyLane.py

    Kernels are implemented as static methods of the QuantumKernel wrapper class

    """

    @staticmethod
    def angle(x, n_wires=4, n_layers=2):
        """
        Kernel function with angle encoding. This circuit will rotate the
        N-dimensional input data into N qubits. Input data can be float numbers.

        Args:
            x:
            n_wires:
            n_layers:

        Returns:

        """
        for _ in range(n_layers):
            AngleEmbedding(x, wires=range(n_wires))

    @staticmethod
    def basis(x, n_wires=4, n_layers=2):
        """
        Kernel function with basis encoding. This circuit will encode N
        input data into N qubits. Input data can only be 0 or 1

        Args:
            x:
            n_wires:
            n_layers:

        Returns:

        """
        BasisEmbedding(x, wires=range(n_wires))

    @staticmethod
    def iqp(x, n_wires=4, n_layers=2):
        """
        Kernel function with IQP encoding. This circuit will encode N
        input data into N qubits. Input data can only be floatnumbers.

        Args:
            x:
            n_wires:
            n_layers:

        Returns:

        """
        IQPEmbedding(x, wires=range(n_wires))

    @staticmethod
    def amplitude(x, n_wires=4, n_layers=2):
        """
        Kernel function with amplitude encoding. This circuit will encode the
        N-dimensional input data into the amplitudes of log(N) qubits.
        Input data can be float numbers.

        Args:
            x:
            n_wires:
            n_layers:

        Returns:

        """
        for _ in range(n_layers):
            AmplitudeEmbedding(x, wires=range(n_wires), pad_with=0)

    @staticmethod
    def homemade_angle(x, n_wires=4, n_layers=2):
        """
        Implementation of the angle embedding as can be found in
        https://github.com/davidfitzek/bachelor_thesis_qml/blob/main/QKE/QKE_PennyLane.py

        Args:
            x:
            n_wires:
            n_layers:

        Returns:

        """
        for i in range(n_wires):
            qml.Hadamard(wires=[i])
            qml.RZ(x[i], wires=[i])
        for i in range(n_wires):
            qml.Hadamard(wires=[i])
            qml.RZ(x[i], wires=[i])

    @staticmethod
    def zz(x, n_wires=4, n_layers=2):
        """
        Implementation of the zz feature map as it can be found in

        Havlíček, Vojtěch; Córcoles, Antonio D.; Temme, Kristan; Harrow, Aram W.;
        Kandala, Abhinav; Chow, Jerry M.; Gambetta, Jay M. (2019):
        Supervised learning with quantum-enhanced feature spaces.
        In: Nature 567 (7747), S. 209–212. DOI: 10.1038/s41586-019-0980-2.

        Args:
            x:
            n_wires:
            n_layers:

        Returns:

        """
        # TODO: error handling might be better if values cannot be set
        for _ in range(n_layers):
            # Bring qubits into superposition
            for i_ in range(n_wires):
                qml.Hadamard(wires=[i_])
                try:
                    qml.RZ(2 * x[i_], wires=[i_])
                except:
                    pass
            # Create entanglement
            for i_ in range(n_wires):
                try:
                    qml.CNOT(wires=[i_ % n_wires, (i_ + 1) % n_wires])
                    qml.RZ(2 * ((math.pi - x[i_ % n_wires]) * (math.pi - x[(i_ + 1) % n_wires])),
                           wires=[(i_ + 1) % n_wires])
                    qml.CNOT(wires=[i_ % n_wires, (i_ + 1) % n_wires])
                except:
                    pass

    @staticmethod
    def deep_zz(x, n_wires=4, n_layers=2):
        for _ in range(n_layers):
            for i_ in range(n_wires):
                qml.Hadamard(wires=[i_])
                qml.RZ(2 * x[i_], wires=[i_])
            for i_ in range(n_wires):
                qml.CNOT(wires=[i_ % n_wires, (i_ + 1) % n_wires])
                qml.RZ(2 * ((math.pi - x[i_ % n_wires]) * (math.pi - x[(i_ + 1) % n_wires]) * (
                        math.pi - x[(i_ + 2) % n_wires])),
                       wires=[(i_ + 1) % n_wires])
                qml.CNOT(wires=[(i_ + 1) % n_wires, (i_ + 2) % n_wires])
                qml.RZ(2 * ((math.pi - x[i_ % n_wires]) * (math.pi - x[(i_ + 1) % n_wires]) * (
                        math.pi - x[(i_ + 2) % n_wires])),
                       wires=[(i_ + 2) % n_wires])
                qml.CNOT(wires=[i_ % n_wires, (i_ + 2) % n_wires])

    @staticmethod
    def zz_angle(x, n_wires=4, n_layers=2):
        """
        Implementation of the zz feature map as it can be found in

        Havlíček, Vojtěch; Córcoles, Antonio D.; Temme, Kristan; Harrow, Aram W.;
        Kandala, Abhinav; Chow, Jerry M.; Gambetta, Jay M. (2019):
        Supervised learning with quantum-enhanced feature spaces.
        In: Nature 567 (7747), S. 209–212. DOI: 10.1038/s41586-019-0980-2.

        Args:
            x:
            n_wires:
            n_layers:

        Returns:

        """
        # TODO: error handling might be better
        for _ in range(n_layers):
            # Bring qubits into superposition
            for i_ in range(n_wires):
                qml.Hadamard(wires=[i_])
                try:
                    qml.RY(2 * x[i_], wires=[i_])
                except:
                    pass
            # Create entanglement
            for i_ in range(n_wires):
                try:
                    qml.CNOT(wires=[i_ % n_wires, (i_ + 1) % n_wires])
                    qml.RZ(2 * ((math.pi - x[i_ % n_wires]) * (math.pi - x[(i_ + 1) % n_wires])),
                           wires=[(i_ + 1) % n_wires])
                    qml.CNOT(wires=[i_ % n_wires, (i_ + 1) % n_wires])
                except:
                    pass
