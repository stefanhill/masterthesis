import datetime
import typing
from functools import reduce

from data.process.Observation import Observation


class Segment:

    def __init__(self, start: typing.Union[str, int], end: typing.Union[str, int]):
        # TODO: replace start and end with activities later
        self._observations = []
        self._batches = []
        self._start = start
        self._end = end

        ### Segment statistics

        # number of batched cases in segment / number of cases in segment
        self._batch_ratio = 0.0

        # durations of all observations in the segment
        self._segment_durations = []
        self._average_segment_duration: 'datetime.timedelta' = datetime.timedelta(0.0)

        # durations of all batches in the segment
        self._batch_durations = []
        self._average_batch_duration: 'datetime.timedelta' = datetime.timedelta(0.0)

    @property
    def observations(self) -> typing.List['Observation']:
        return self._observations

    @observations.setter
    def observations(self, value: typing.List['Observation']):
        self._observations = value
        self.update_batch_ratio()

        self._segment_durations = []
        for observation in self._observations:
            self._segment_durations.append(observation.end.timestamp - observation.start.timestamp)
        self.update_average_segment_duration()

    @property
    def batches(self):
        return self._batches

    @batches.setter
    def batches(self, value):
        self._batches = value
        self.update_batch_ratio()

        self._batch_durations = []
        for batch in self._batches:
            if len(batch) == 0:
                self._batch_durations.append(0.0)
            else:
                self._batch_durations.append(batch[-1].end.timestamp - batch[0].start.timestamp)
        self.update_average_batch_duration()

    @property
    def batch_ratio(self) -> float:
        return self._batch_ratio

    @batch_ratio.setter
    def batch_ratio(self, value: float):
        self._batch_ratio = value

    @property
    def average_segment_duration(self) -> 'datetime.timedelta':
        return self._average_segment_duration

    @average_segment_duration.setter
    def average_segment_duration(self, value: 'datetime.timedelta'):
        self._average_segment_duration = value

    @property
    def average_batch_duration(self) -> 'datetime.timedelta':
        return self._average_batch_duration

    @average_batch_duration.setter
    def average_batch_duration(self, value: 'datetime.timedelta'):
        self._average_batch_duration = value

    @property
    def start(self):
        return self._start

    @start.setter
    def start(self, value):
        self._start = value

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, value):
        self._end = value

    def update_batch_ratio(self):
        if len(self.observations) == 0:
            self._batch_ratio = 0.0
        else:
            self._batch_ratio = reduce(lambda count, l: count + len(l), self.batches, 0) \
                                / len(self.observations)

    def update_average_segment_duration(self):
        if len(self._segment_durations) == 0:
            self._average_segment_duration: 'datetime.timedelta' = datetime.timedelta(0.0)
        else:
            self._average_segment_duration = sum(self._segment_durations, datetime.timedelta()) \
                                             / len(self._segment_durations)

    def update_average_batch_duration(self):
        if len(self._batch_durations) == 0:
            self._average_batch_duration: 'datetime.timedelta' = datetime.timedelta(0.0)
        else:
            self._average_batch_duration = sum(self._batch_durations, datetime.timedelta()) \
                                           / len(self._batch_durations)
