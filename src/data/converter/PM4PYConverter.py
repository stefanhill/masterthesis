from data.AbstractConverter import AbstractConverter
from data.dataset.EndEvent import EndEvent
from data.dataset.Event import Event
from data.dataset.EventLog import EventLog
from data.dataset.StartEvent import StartEvent
from data.dataset.Trace import Trace


class PM4PYConverter(AbstractConverter):

    def __init__(self, *args, wrap_trace: bool = True):
        super().__init__(*args)
        self._wrap_trace = wrap_trace

    def to_event_log(self) -> 'EventLog':
        event_log = EventLog()
        traces = []
        trace_id = 0
        for t in getattr(self._dataset, '_list'):
            trace = Trace(
                identifier=trace_id if 'concept:name' not in t.attributes.keys() else t.attributes['concept:name'],
                attributes=t.attributes)
            events = []
            event_identifier = 0
            for i, e in enumerate(getattr(t, '_list')):
                e_attrs = getattr(e, '_dict')
                if i == 0 and self._wrap_trace:
                    events.append(StartEvent(identifier=event_identifier,
                                             attributes=e_attrs))
                    event_identifier += 1
                events.append(Event(identifier=event_identifier,
                                    attributes=e_attrs))
                event_identifier += 1
                if i == len(getattr(t, '_list')) - 1 and self._wrap_trace:
                    events.append(EndEvent(attributes=e_attrs))
            trace.events = events
            traces.append(trace)
            trace_id += 1
        event_log.traces = traces
        return event_log
