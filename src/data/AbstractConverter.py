import abc
import typing


class AbstractConverter(abc.ABC):

    def __init__(self, dataset: typing.Any):
        self._dataset = dataset
