import abc
import typing


class AbstractProcessor(abc.ABC):

    @abc.abstractmethod
    def process(self, data: typing.Any) -> typing.Any:
        pass
