import abc

from data.dataset.EventLog import EventLog
from data.processing.AbstractFilter import AbstractFilter


class AbstractEventLogFilter(AbstractFilter, abc.ABC):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @abc.abstractmethod
    def process(self, data: 'EventLog') -> 'EventLog':
        pass
