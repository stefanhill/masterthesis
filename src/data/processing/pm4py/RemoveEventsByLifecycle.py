import typing

import pm4py

from data.processing.AbstractProcessor import AbstractProcessor
from data.processing.pm4py.RemoveEventsByIndex import RemoveEventsByIndex


class RemoveEventsByLifecycle(AbstractProcessor):

    def __init__(self, is_manual: bool = True, lifecycles: typing.List[str] = None):
        self._is_manual = is_manual
        self._lifecycles = [] if lifecycles is None else lifecycles

    def process(self, trace: 'pm4py.objects.log.obj.Trace') -> 'pm4py.objects.log.obj.Trace':
        remove_indices = []
        for i, event in enumerate(getattr(trace, '_list')):
            event_properties = getattr(event, '_dict')
            if event['lifecycle:transition'] in self._lifecycles:
                if not self._is_manual or event_properties['concept:name'].startswith('W'):
                    remove_indices.append(i)
        return RemoveEventsByIndex(indices=remove_indices).process(trace)
