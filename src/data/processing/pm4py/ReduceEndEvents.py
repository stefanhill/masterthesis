import typing

import pm4py

from data.processing.AbstractProcessor import AbstractProcessor
from data.processing.pm4py.RemoveEventsByIndex import RemoveEventsByIndex


class ReduceEndEvents(AbstractProcessor):

    def __init__(self, keep: typing.List[str] = None):
        self._keep = [] if keep is None else keep

    def process(self, trace: 'pm4py.objects.log.obj.Trace') -> 'pm4py.objects.log.obj.Trace':
        last_index = -1
        for i, event in enumerate(getattr(trace, '_list')):
            event_properties = getattr(event, '_dict')
            if event_properties['concept:name'] in self._keep:
                last_index = i
        if last_index == -1:
            return trace
        else:
            remove_indices = list(range(last_index + 1, len(getattr(trace, '_list'))))
            return RemoveEventsByIndex(indices=remove_indices).process(trace)
