import pm4py

from data.processing.AbstractProcessor import AbstractProcessor


class LifecycleToName(AbstractProcessor):

    def __init__(self, is_manual: bool = True):
        self._is_manual = is_manual

    def process(self, trace: 'pm4py.objects.log.obj.Trace') -> 'pm4py.objects.log.obj.Trace':
        temp_list = getattr(trace, '_list')
        for event in temp_list:
            event_properties = getattr(event, '_dict')
            if not self._is_manual or event_properties['concept:name'].startswith('W'):
                event_properties['concept:name'] = \
                    f"{event_properties['concept:name']}_{event_properties['lifecycle:transition']}"
            setattr(event, '_dict', event_properties)
        setattr(trace, '_list', temp_list)
        return trace
