import typing
from datetime import datetime

import pm4py

from data.processing.AbstractProcessor import AbstractProcessor
from data.processing.pm4py.RemoveEventsByIndex import RemoveEventsByIndex


class ManualRemoveEvents(AbstractProcessor):

    def __init__(self):
        pass

    def process(self, trace: 'pm4py.objects.log.obj.Trace') -> 'pm4py.objects.log.obj.Trace':
        """

        Only works for BPI Challenge 2012 log, because of previous data analysis

        Args:
            trace:

        Returns:

        """
        event_names: typing.List[str] = list(
            map(lambda e: getattr(e, '_dict')['concept:name'], getattr(trace, '_list')))
        event_timestamps: typing.List[datetime] = list(
            map(lambda e: getattr(e, '_dict')['time:timestamp'], getattr(trace, '_list')))
        o_index, a_index = None, None
        if 'O_CANCELLED' in event_names:
            o_index = len(event_names) - 1 - event_names[::-1].index('O_CANCELLED')
        if 'A_CANCELLED' in event_names:
            a_index = len(event_names) - 1 - event_names[::-1].index('A_CANCELLED')
        if o_index is None or a_index is None:
            return trace
        else:
            if abs((event_timestamps[a_index] - event_timestamps[o_index]).total_seconds()) > 10:
                return trace
            else:
                return RemoveEventsByIndex(indices=[o_index]).process(trace)
