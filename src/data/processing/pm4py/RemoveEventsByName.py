import typing

import pm4py

from data.processing.AbstractProcessor import AbstractProcessor
from data.processing.pm4py.RemoveEventsByIndex import RemoveEventsByIndex


class RemoveEventsByName(AbstractProcessor):

    def __init__(self, names: typing.List[str] = None):
        self._names = [] if names is None else names

    def process(self, trace: 'pm4py.objects.log.obj.Trace') -> 'pm4py.objects.log.obj.Trace':
        remove_indices = []
        for i, event in enumerate(getattr(trace, '_list')):
            event_properties = getattr(event, '_dict')
            if event_properties['concept:name'] in self._names:
                remove_indices.append(i)
        return RemoveEventsByIndex(indices=remove_indices).process(trace)
