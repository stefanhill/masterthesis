import typing

import pm4py

from data.processing.AbstractProcessor import AbstractProcessor


class RemoveEventsByIndex(AbstractProcessor):

    def __init__(self, is_manual: bool = True, indices: typing.List[int] = None):
        self._is_manual = is_manual
        self._indices = [] if indices is None else indices

    def process(self, trace: 'pm4py.objects.log.obj.Trace') -> 'pm4py.objects.log.obj.Trace':
        temp_list = getattr(trace, '_list')
        for i in sorted(self._indices, reverse=True):
            temp_list.pop(i)
        setattr(trace, '_list', temp_list)
        return trace
