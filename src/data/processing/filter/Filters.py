import pm4py


class Filters:

    @staticmethod
    def remove_no_user(trace: 'pm4py.objects.log.obj.Trace'):
        try:
            list(map(lambda event: getattr(event, '_dict')['org:resource'], getattr(trace, '_list')))
            return True
        except KeyError:
            return False

    @staticmethod
    def end_events(trace: 'pm4py.objects.log.obj.Trace'):
        event_names = list(map(lambda event: getattr(event, '_dict')['concept:name'], getattr(trace, '_list')))
        for event_name in ['A_DECLINED', 'A_APPROVED', 'A_CANCELLED']:
            if event_name in event_names:
                return True
        return False

