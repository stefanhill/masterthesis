import abc

from data.dataset.EventLog import EventLog
from data.processing.AbstractProcessor import AbstractProcessor


class AbstractEventLogProcessor(AbstractProcessor, abc.ABC):

    @abc.abstractmethod
    def process(self, data: 'EventLog') -> 'EventLog':
        pass
