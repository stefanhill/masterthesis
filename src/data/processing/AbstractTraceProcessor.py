import abc

from data.dataset.Trace import Trace
from data.processing.AbstractProcessor import AbstractProcessor


class AbstractTraceProcessor(AbstractProcessor, abc.ABC):

    @abc.abstractmethod
    def process(self, data: 'Trace') -> 'Trace':
        pass
