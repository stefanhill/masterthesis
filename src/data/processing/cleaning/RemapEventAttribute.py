import typing

from data.dataset.Trace import Trace
from data.processing.AbstractTraceProcessor import AbstractTraceProcessor


class RemapEventAttribute(AbstractTraceProcessor):

    def __init__(self, name_mapping: typing.Dict[str, str], attribute_name: str = 'concept:name'):
        self._name_mapping = name_mapping
        self._attribute_name = attribute_name

    def process(self, trace: 'Trace') -> 'Trace':
        for event in trace.events:
            if self._attribute_name in event.attributes.keys() \
                    and event.attributes[self._attribute_name] in self._name_mapping.keys():
                event.attributes[self._attribute_name] = self._name_mapping[event.attributes[self._attribute_name]]
        return trace
