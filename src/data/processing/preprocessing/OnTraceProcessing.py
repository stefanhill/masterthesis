import typing

from data.dataset.EventLog import EventLog
from data.processing.AbstractEventLogProcessor import AbstractEventLogProcessor
from data.processing.AbstractTraceProcessor import AbstractTraceProcessor


class OnTraceProcessing(AbstractEventLogProcessor):

    def __init__(self, trace_processors: typing.List['AbstractTraceProcessor']):
        self._trace_processors = trace_processors

    def process(self, data: 'EventLog') -> 'EventLog':
        for trace in data.traces:
            for trace_processor in self._trace_processors:
                trace = trace_processor.process(trace)
        return data
