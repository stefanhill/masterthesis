import random

from data.dataset.EventLog import EventLog
from data.processing.AbstractEventLogProcessor import AbstractEventLogProcessor


class RandomTraceCut(AbstractEventLogProcessor):

    def __init__(self, min_trace_length: int = 4, remove_end_events: bool = True):
        self._min_trace_length = min_trace_length
        self._remove_end_events = remove_end_events

    def process(self, data: 'EventLog') -> 'EventLog':
        for trace in data.traces:
            if self._remove_end_events:
                trace.events.pop()
            i = len(trace)
            stop_index = random.choice(range(self._min_trace_length, len(trace))) \
                if len(trace) > self._min_trace_length \
                else self._min_trace_length
            while i > self._min_trace_length and i > stop_index:
                trace.events.pop()
                i = i - 1
        return data
