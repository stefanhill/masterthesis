import abc
import copy
import typing

from data import AbstractDataInstance


class AbstractDataset(abc.ABC):

    def __init__(self):
        self._instances = []

    @property
    def instances(self) -> typing.List['AbstractDataInstance']:
        return self._instances

    @instances.setter
    def instances(self, value: typing.List['AbstractDataInstance']):
        self._instances = value

    def __len__(self):
        return len(self.instances)

    def __getitem__(self, item):
        dataset = copy.deepcopy(self)
        if isinstance(item, list):
            dataset.instances = [dataset.instances[i] for i in item]
        elif isinstance(item, slice):
            dataset.instances = dataset.instances[item]
        elif isinstance(item, int):
            dataset.instances = [dataset.instances[item]]
        return dataset
