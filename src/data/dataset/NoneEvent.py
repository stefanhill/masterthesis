from data.dataset.Event import Event


class NoneEvent(Event):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = '__NONE__'
        self._attributes = {}
