import datetime
import typing

from data.AbstractDataPoint import AbstractDataPoint


class Event(AbstractDataPoint):

    def __init__(self,
                 identifier=None,
                 timestamp: datetime.datetime = None,
                 attributes: typing.Dict[str, typing.Any] = None):
        super().__init__()

        if identifier is not None:
            self._identifier = identifier
        elif attributes is not None and 'concept:name' in attributes.keys():
            self._identifier = attributes['concept:name']

        if timestamp is not None:
            self._timestamp = timestamp
        elif attributes is not None and 'time:timestamp' in attributes.keys():
            self._timestamp = attributes['time:timestamp']

        self._attributes = attributes

    @property
    def identifier(self):
        return self._identifier

    @identifier.setter
    def identifier(self, value):
        self._identifier = value

    @property
    def attributes(self):
        return self._attributes

    @attributes.setter
    def attributes(self, value):
        self._attributes = value

    @property
    def timestamp(self) -> datetime.datetime:
        return self._timestamp

    @timestamp.setter
    def timestamp(self, value: datetime.datetime):
        self._timestamp = value
