from data.dataset.Event import Event


class EndEvent(Event):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = '__END__'
        self._attributes['concept:name'] = '__END__'
