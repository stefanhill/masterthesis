from data.dataset.Event import Event


class StartEvent(Event):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.name = '__START__'
        self._attributes['concept:name'] = '__START__'
