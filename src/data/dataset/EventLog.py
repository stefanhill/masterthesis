import typing
from collections import OrderedDict

from deprecation import deprecated

from data.AbstractDataset import AbstractDataset
from data.dataset.Trace import Trace


class EventLog(AbstractDataset):

    def __init__(self):
        super().__init__()
        self._instances: typing.Dict[typing.Any, 'Trace'] = OrderedDict()

    @property
    def traces(self) -> typing.List['Trace']:
        return list(self._instances.values())

    @traces.setter
    def traces(self, value: typing.List['Trace']):
        for trace in value:
            self._instances[trace.identifier] = trace

    def get_trace_by_id(self, trace_identifier) -> 'Trace':
        return self._instances[trace_identifier]

    @deprecated
    def get_possible_features(self) -> typing.Tuple[typing.List[str], typing.List[str]]:
        # TODO: use exception instead, no hardcode on first element
        assert len(self.traces) > 0
        assert len(self.traces[0].events) > 0
        return list(getattr(self.traces[0].events[1], '_attributes').keys()), list(
            getattr(self.traces[0], '_attributes').keys())

    @deprecated
    def get_possible_targets(self) -> typing.Tuple[typing.List[str], typing.List[str]]:
        # TODO: use exception instead, no hardcode on first element
        assert len(self.traces) > 0
        assert len(self.traces[0].events) > 0
        return list(getattr(self.traces[0].events[1], '_attributes').keys()), list(
            getattr(self.traces[0], '_attributes').keys())
