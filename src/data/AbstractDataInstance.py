import abc
import copy
import typing

from data.AbstractDataPoint import AbstractDataPoint


class AbstractDataInstance(abc.ABC):

    def __init__(self):
        self._points = []

    @property
    def points(self) -> typing.List['AbstractDataPoint']:
        return self._points

    @points.setter
    def points(self, value: typing.List['AbstractDataPoint']):
        self._points = value

    def __len__(self):
        return len(self.points)

    def __getitem__(self, item):
        data_instance = copy.deepcopy(self)
        if isinstance(item, list):
            data_instance.points = [data_instance.points[i] for i in item]
        elif isinstance(item, slice):
            data_instance.points = data_instance.points[item]
        elif isinstance(item, int):
            data_instance.points = [data_instance.points[item]]
        return data_instance
