import enum


class DataType(enum.Enum):
    """Abstract Data type, Might not be necessary

    """
    pass


class PredictionType(enum.Enum):
    """Prediction types for the classifiers

    """
    ACTIVITY = 'ACTIVITY'
    TIME = 'TIME'
