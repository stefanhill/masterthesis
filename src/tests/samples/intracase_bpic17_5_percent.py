import datetime
import logging
import pickle
import sys

import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_validate
from sklearn.svm import LinearSVC, SVC
from sklearn.utils import resample
from xgboost import XGBClassifier

sys.path.append('/home/david/masterthesis-quantum-ppm/masterthesis/src/')

from data.handler.EventLogHandler import EventLogHandler
from ml.classifier.NGram import NGram
from ml.classifier.QKE import QKE
from ml.classifier.quantum.QuantumEmbedding import QuantumEmbedding
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)
np.random.seed(0)
MEDIAN_CASE_TIME = 27.1
EXPERIMENT_ID = str(datetime.datetime.now().strftime('%Y-%m-%dT%H-%M-%S'))
EXPERIMENT_NAME = 'intracase_bpic17_5_percent'
metrics = {}

handler = EventLogHandler()
handler.load(str(Path(['resources', 'test_logs', 'bpi2017_small.xes'])))

for index_ in [2]:#, 4, 6]:

    MAX_CLASSES = len(handler.process_definition)
    encoder_name = f'index_based_{index_}'
    print(f'Build encoding {encoder_name}')

    builder = EventLogEncodingBuilder() \
        .add(IndexBasedEncoder(window=index_))


    classifiers = {
        'ngram': NGram(max_classes=MAX_CLASSES),
        'random_forest_3': RandomForestClassifier(max_depth=3, random_state=0),
        'random_forest_4': RandomForestClassifier(max_depth=4, random_state=0),
        'xgboost': XGBClassifier(),
        'svc_linear': LinearSVC(),
        'svc_rbf': SVC(kernel='rbf'),
        # 'vqc_angle_2': VQC(n_layers=2, state_preparation=QuantumEmbedding.angle, epochs=20,
        #                    min_wires=MAX_CLASSES, use_jax=False, debug=True),
        # 'vqc_zz_2': VQC(n_layers=2, state_preparation=QuantumEmbedding.zz, epochs=20,
        #                 min_wires=MAX_CLASSES, use_jax=False, debug=True),
        # 'vqc_zz_angle_2': VQC(n_layers=2, state_preparation=QuantumEmbedding.zz_angle, epochs=20,
        #                       min_wires=MAX_CLASSES, use_jax=False, debug=True),
        'qke_angle_1': QKE(n_layers=1, kernel=QuantumEmbedding.angle, use_jax=True, debug=True),
        'qke_angle_2': QKE(n_layers=2, kernel=QuantumEmbedding.angle, use_jax=True, debug=True),
        'qke_zz_1': QKE(n_layers=1, kernel=QuantumEmbedding.zz, use_jax=True, debug=True),
        # 'qke_zz_2': QKE(n_layers=2, kernel=QuantumEmbedding.zz, use_jax=True, debug=True),
        # 'qke_zz_angle_1': QKE(n_layers=1, kernel=QuantumEmbedding.zz_angle, use_jax=True, debug=True),
        # 'qke_zz_angle_2': QKE(n_layers=2, kernel=QuantumEmbedding.zz_angle, use_jax=True, debug=True),
    }

    metrics[encoder_name] = {}

    print(f'Start to build encoder {encoder_name}')
    X_full, y_full = builder.build(handler)

    X, y = resample(X_full, y_full, n_samples=int(len(y_full) * 0.05), replace=False, stratify=y_full, random_state=0)

    for clf_name, clf in classifiers.items():
        print(f'Starting cross validation of {clf_name} on encoding {encoder_name}')
        metrics[encoder_name][clf_name] = cross_validate(clf, X, y, cv=3)
        print(metrics[encoder_name][clf_name])

        pickle.dump(metrics,
                    open(str(Path(['resources', 'test_results', f'{EXPERIMENT_ID}_{EXPERIMENT_NAME}.metrics'])),
                         'wb'))
        del clf_name, clf
