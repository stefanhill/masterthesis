import copy
import datetime
import logging
import pickle
import sys

import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_validate
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.svm import LinearSVC, SVC
from xgboost import XGBClassifier

sys.path.append('/home/david/masterthesis-quantum-ppm/masterthesis/src/')

from ml.encoding.intercase.AverageDelay import AverageDelay
from ml.encoding.intercase.FrequentPreviousActivity import FrequentPreviousActivity
from ml.encoding.intercase.FutureBatchingBehaviour import FutureBatchingBehaviour
from ml.encoding.intercase.NoPeerCases import NoPeerCases
from ml.encoding.intercase.PeerActivityCount import PeerActivityCount
from ml.encoding.intercase.ResourceCount import ResourceCount
from ml.encoding.intercase.TopBusyResource import TopBusyResource

from data.handler.EventLogHandler import EventLogHandler
from ml.classifier.NGram import NGram
from ml.classifier.VQC import VQC
from ml.classifier.QKE import QKE
from ml.classifier.quantum.QuantumEmbedding import QuantumEmbedding
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)
np.random.seed(0)
MEDIAN_CASE_TIME = 27.1
EXPERIMENT_ID = str(datetime.datetime.now().strftime('%Y-%m-%dT%H-%M-%S'))
EXPERIMENT_NAME = 'intercase_bpic11'
metrics = {}

handler = EventLogHandler()
handler.load(str(Path(['resources', 'test_logs', 'bpi2011_small.xes'])))

timedeltas = [datetime.timedelta(weeks=0.15 * MEDIAN_CASE_TIME),
              datetime.timedelta(weeks=0.3 * MEDIAN_CASE_TIME),
              datetime.timedelta(weeks=0.5 * MEDIAN_CASE_TIME)]

i_ = 0
for timedelta in timedeltas:
    print(f'Inter-case encoding window on {timedelta}')

    handler_ = copy.deepcopy(handler)

    handler_.mine_batching_patterns(min_batch_size=10, max_delay=0.1 * timedelta)

    ngram = NGram(max_classes=len(handler_.process_definition))
    X_ngram, y_ngram = EventLogEncodingBuilder() \
        .add(IndexBasedEncoder(window=4)) \
        .build(handler_)

    ngram.fit(X_ngram, y_ngram)

    single_encoders = {
        'peer_cases': [NoPeerCases(timeframe=timedelta)],
        'peer_act': [PeerActivityCount(timeframe=timedelta)],
        'res_count': [ResourceCount(timeframe=timedelta)],
        'avg_delay': [AverageDelay(timeframe=timedelta)],
        'freq_act': [FrequentPreviousActivity(timeframe=timedelta, no_activities=2,
                                              normalization=True)],
        'top_res': [TopBusyResource(timeframe=timedelta, no_resources=2, normalization=True)],
        'batch': [FutureBatchingBehaviour(segment_classifier=ngram,
                                          include_average_times=False)]
    }

    # dual_encoders = {}
    #
    # for name_1, enc_1, in single_encoders.items():
    #     for name_2, enc_2 in single_encoders.items():
    #         if name_1 != name_2 and f'{name_2}+{name_1}' not in dual_encoders.keys():
    #             dual_encoders[f'{name_1}+{name_2}'] = enc_1 + enc_2
    #
    # all_encoders = {
    #     'all': list(reduce(lambda a, b: a + b, single_encoders.values()))
    # }

    MAX_CLASSES = len(handler.process_definition)

    for encoder_name, encoders in single_encoders.items():

        normalize = encoder_name in ['peer_cases', 'peer_act', 'res_count', 'avg_delay']

        encoder_name = f'{int(timedelta.total_seconds())}+{encoder_name}'

        builder = EventLogEncodingBuilder() \
            .add(IndexBasedEncoder(window=4, normalization=True))
        for encoder in encoders:
            builder.add(encoder)

        classifiers = {
            'random_forest_3': RandomForestClassifier(max_depth=3, random_state=0),
            'random_forest_4': RandomForestClassifier(max_depth=4, random_state=0),
            'xgboost': XGBClassifier(),
            'svc_linear': LinearSVC(),
            'svc_rbf': SVC(kernel='rbf'),
            'vqc_angle_2': VQC(n_layers=2, state_preparation=QuantumEmbedding.angle, epochs=20,
                               min_wires=MAX_CLASSES, use_jax=False, debug=True),
            'vqc_zz_2': VQC(n_layers=2, state_preparation=QuantumEmbedding.zz, epochs=20,
                            min_wires=MAX_CLASSES, use_jax=False, debug=True),
            'vqc_zz_angle_2': VQC(n_layers=2, state_preparation=QuantumEmbedding.zz_angle, epochs=20,
                                  min_wires=MAX_CLASSES, use_jax=False, debug=True),
            'qke_angle_1': QKE(n_layers=1, kernel=QuantumEmbedding.angle, use_jax=True, debug=True),
            'qke_angle_2': QKE(n_layers=2, kernel=QuantumEmbedding.angle, use_jax=True, debug=True),
            'qke_zz_1': QKE(n_layers=1, kernel=QuantumEmbedding.zz, use_jax=True, debug=True),
            'qke_zz_2': QKE(n_layers=2, kernel=QuantumEmbedding.zz, use_jax=True, debug=True),
            'qke_zz_angle_1': QKE(n_layers=1, kernel=QuantumEmbedding.zz_angle, use_jax=True, debug=True),
            'qke_zz_angle_2': QKE(n_layers=2, kernel=QuantumEmbedding.zz_angle, use_jax=True, debug=True),
        }

        metrics[encoder_name] = {}

        print(f'Start to build encoder {encoder_name}')
        X, y = builder.build(handler)

        if normalize:
            X = MinMaxScaler().fit_transform(X)

        for clf_name, clf in classifiers.items():
            print(f'Starting cross validation of {clf_name} on encoding {encoder_name}')
            metrics[encoder_name][clf_name] = cross_validate(clf, X, y, cv=3)
            print(metrics[encoder_name][clf_name])

            pickle.dump(metrics,
                        open(str(Path(['resources', 'test_results', f'{EXPERIMENT_ID}_{EXPERIMENT_NAME}.metrics'])),
                             'wb'))
            del clf_name, clf
    i_ += 1

    del handler_
