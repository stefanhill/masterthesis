import copy
import datetime
import logging
import pickle
import sys
import time
from functools import reduce

sys.path.append('/home/thesis/masterthesis/src')

from data.handler.EventLogHandler import EventLogHandler
from ml.MLUtils import MLUtils
from ml.classifier.NGram import NGram
from workbench.deprecated.QKE_jax import QKE
from ml.classifier.quantum.QuantumEmbedding import QuantumEmbedding
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intercase.AverageDelay import AverageDelay
from ml.encoding.intercase.FrequentPreviousActivity import FrequentPreviousActivity
from ml.encoding.intercase.FutureBatchingBehaviour import FutureBatchingBehaviour
from ml.encoding.intercase.NoPeerCases import NoPeerCases
from ml.encoding.intercase.PeerActivityCount import PeerActivityCount
from ml.encoding.intercase.ResourceCount import ResourceCount
from ml.encoding.intercase.TopBusyResource import TopBusyResource
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)
metrics = {}

train_handler = EventLogHandler()
train_handler.load(str(Path(['resources', 'test_logs', 'bpi2011_train.xes'])))

val_handler = EventLogHandler()
val_handler.load(str(Path(['resources', 'test_logs', 'bpi2011_validation.xes'])))

# Ensure the mapping is the same for train and validation handler
val_handler.process_definition = train_handler.process_definition

timedeltas = [datetime.timedelta(weeks=0.25 * 27.1)]#, datetime.timedelta(weeks=0.5 * 27.1),
              #datetime.timedelta(weeks=0.75 * 27.1)]
# min_batch_sizes = [10]
# max_delays = list(map(lambda x: 0.1 * x, timedeltas))

i_ = 0
for timedelta in timedeltas:
    # for min_batch_size in min_batch_sizes:
    #     for max_delay in max_delays:
    print(f'{i_} ## {timedelta}')

    train_handler_ = copy.deepcopy(train_handler)
    val_handler_ = copy.deepcopy(val_handler)

    train_handler_.mine_batching_patterns(min_batch_size=10, max_delay=0.1 * timedelta)
    val_handler_.mine_batching_patterns(min_batch_size=10, max_delay=0.1 * timedelta)

    ngram = NGram(n=4)
    ngram.fit(train_handler_)

    norm_no_peer_cases = MLUtils.autonormalize_encoder(NoPeerCases(timeframe=timedelta), train_handler_,
                                                       val_handler_)
    norm_peer_activity_count = MLUtils.autonormalize_encoder(PeerActivityCount(timeframe=timedelta),
                                                             train_handler_, val_handler_)
    norm_resource_count = MLUtils.autonormalize_encoder(ResourceCount(timeframe=timedelta), train_handler_,
                                                        val_handler_)
    norm_average_delay = MLUtils.autonormalize_encoder(AverageDelay(timeframe=timedelta), train_handler_,
                                                       val_handler_)

    norm_freq_previous_activity = FrequentPreviousActivity(timeframe=timedelta, no_activities=2,
                                                           normalization=True)
    norm_top_busy_resource = TopBusyResource(timeframe=timedelta, no_resources=2, normalization=True)
    norm_future_batching_behaviour = FutureBatchingBehaviour(segment_classifier=ngram,
                                                             include_average_times=False)

    single_encoders = {
        'no_peer_cases': [norm_no_peer_cases],
        'peer_activity_count': [norm_peer_activity_count],
        'resource_count': [norm_resource_count],
        'average_delay': [norm_average_delay],
        'freq_previous_activity': [norm_freq_previous_activity],
        'top_busy_resource': [norm_top_busy_resource],
        'future_batching_behaviour': [norm_future_batching_behaviour]
    }

    dual_encoders = {}

    for name_1, enc_1, in single_encoders.items():
        for name_2, enc_2 in single_encoders.items():
            if name_1 != name_2 and f'{int(timedelta.total_seconds())}+{name_2}+{name_1}' not in dual_encoders.keys():
                dual_encoders[f'{int(timedelta.total_seconds())}+{name_1}+{name_2}'] = enc_1 + enc_2

    all_encoders = {
        'all': list(reduce(lambda a, b: a + b, single_encoders.values()))
    }

    #encoders = {**single_encoders, **dual_encoders, **all_encoders}
    encoders = {**all_encoders}

    for encoder_name, encoder_list in encoders.items():

        builder = EventLogEncodingBuilder() \
            .add(IndexBasedEncoder(window=4))
        for encoder in encoder_list:
            builder.add(encoder)

        classifiers = {
            #'random_forest_3': SKWrapper(model=RandomForestClassifier(max_depth=3, random_state=0),
             #                            encoder=builder),
            #'random_forest_4': SKWrapper(model=RandomForestClassifier(max_depth=4, random_state=0),
              #                           encoder=builder),
            #'xgboost': SKWrapper(model=XGBClassifier(), encoder=builder),
            #'svc_linear': SKWrapper(model=LinearSVC(), encoder=builder),
            #'svc_rbf': SKWrapper(model=SVC(kernel='rbf'), encoder=builder),
            'qke_zz_2': QKE(n_layers=2, kernel=QuantumEmbedding.zz, encoder=builder)
        }

        metrics[encoder_name] = {}

        for clf_name, clf in classifiers.items():
            logging.warning(f'Starting training of {clf_name} on encoding {encoder_name}')
            train_time_start = time.time()
            clf.fit(train_handler)
            train_time_end = time.time()
            logging.warning(f'Train time: {train_time_end - train_time_start}')
            val_time_start = time.time()
            metric = clf.evaluate(val_handler)
            val_time_end = time.time()
            logging.warning(f'Validation time: {val_time_end - val_time_start}')
            logging.warning(f'Validation accuracy {metric.accuracy()}')
            metrics[encoder_name][clf_name] = {
                'metric': metric,
                'train_time': train_time_end - train_time_start,
                'val_time': val_time_end - val_time_start
            }
            pickle.dump(metrics,
                        open(str(Path(['resources', 'test_results', 'intercase_bpic11_jax.metrics'])), 'wb'))
            del clf_name, clf

    del train_handler_, val_handler_
    i_ += 1