import datetime
import logging
import sys

import pandas as pd

sys.path.append('/home/david/masterthesis-quantum-ppm/masterthesis/src/')

from data.handler.EventLogHandler import EventLogHandler
from ml.encoding.intercase.AverageDelay import AverageDelay
from ml.encoding.intercase.FrequentPreviousActivity import FrequentPreviousActivity
from ml.encoding.intercase.NoPeerCases import NoPeerCases
from ml.encoding.intercase.PeerActivityCount import PeerActivityCount
from ml.encoding.intercase.ResourceCount import ResourceCount
from utils.Path import Path

logging.basicConfig(level=logging.WARNING)

handler = EventLogHandler()
handler.load(str(Path(['resources', 'test_logs', 'rtfm_small.xes'])))


def get_full_dataframe(handler_, timedelta_):
    builder = EventLogEncodingBuilder() \
        .add(NoPeerCases(timeframe=timedelta_)) \
        .add(PeerActivityCount(timeframe=timedelta_)) \
        .add(ResourceCount(timeframe=timedelta_)) \
        .add(AverageDelay(timeframe=timedelta_)) \
        .add(FrequentPreviousActivity(timeframe=timedelta_, no_activities=2, normalization=True))

    X, y = builder.build(handler_)

    df_values = {}

    df_values['peer_cases'] = X[:, 0]
    df_values['peer_act'] = X[:, 1]
    df_values['res_count'] = X[:, 2]
    df_values['avg_delay'] = X[:, 3]
    df_values['freq_act_1'] = X[:, 4]
    df_values['freq_act_2'] = X[:, 5]
    df_values['y'] = y

    df_ = pd.DataFrame(df_values)

    def min_max_scaling(series):
        return (series - series.min()) / (series.max() - series.min())

    for col in ['peer_cases', 'peer_act', 'res_count', 'avg_delay']:
        df_[col] = min_max_scaling(df_[col])

    return df_


MEDIAN_CASE_TIME = 20.4

from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder

timedelta = datetime.timedelta(weeks=0.5 * MEDIAN_CASE_TIME)
print('Building features')
df = get_full_dataframe(handler, timedelta)

df.to_pickle('rtfm_intercase.features')
