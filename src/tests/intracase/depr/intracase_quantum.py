import logging
import pickle
import time

from data.handler.EventLogHandler import EventLogHandler
from ml.classifier.VQC import VQC
from ml.classifier.quantum.QuantumEmbedding import QuantumEmbedding
from ml.classifier.quantum.QuantumLayer import QuantumLayer
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)
metrics = {}

train_handler = EventLogHandler()
train_handler.load(str(Path(['resources', 'test_logs', 'bpi2011_train.xes'])))

val_handler = EventLogHandler()
val_handler.load(str(Path(['resources', 'test_logs', 'bpi2011_validation.xes'])))

# Ensure the mapping is the same for train and validation handler
val_handler.process_definition = train_handler.process_definition

encoders = {}

for i in range(3, 4):
    encoders[f'index_based_{i}'] = EventLogEncodingBuilder() \
        .add(IndexBasedEncoder(window=i, normalization=True))

#encoders['binary_aggregation'] = EventLogEncodingBuilder() \
#    .add(AggregationEncoder(mode='binary'))

for encoder_name, encoder in encoders.items():
    metrics[encoder_name] = {}
    classifiers = {
        # 'ngram': NGram(encoder=encoder),
        # 'random_forest_2': SKWrapper(model=RandomForestClassifier(max_depth=2, random_state=0), encoder=encoder),
        # 'random_forest_3': SKWrapper(model=RandomForestClassifier(max_depth=3, random_state=0), encoder=encoder),
        # 'random_forest_4': SKWrapper(model=RandomForestClassifier(max_depth=4, random_state=0), encoder=encoder),
        # 'xgboost': SKWrapper(model=XGBClassifier(), encoder=encoder),
        # 'vqc_angle_2a': VQC(n_layers=2, layer=QuantumLayer.default_variational,
        #                     state_preparation=QuantumEmbedding.angle,
        #                     encoder=encoder),
        # 'vqc_angle_2b': VQC(n_layers=2, layer=QuantumLayer.default_variational,
        #                     state_preparation=QuantumEmbedding.angle,
        #                     encoder=encoder),
        # 'vqc_angle_2c': VQC(n_layers=2, layer=QuantumLayer.default_variational,
        #                     state_preparation=QuantumEmbedding.angle,
        #                     encoder=encoder),
        'vqc_zz_2a': VQC(n_layers=2, layer=QuantumLayer.default_variational,
                         state_preparation=QuantumEmbedding.zz,
                         encoder=encoder, epochs=10),
        'vqc_zz_2b': VQC(n_layers=2, layer=QuantumLayer.default_variational,
                         state_preparation=QuantumEmbedding.zz,
                         encoder=encoder, epochs=20),
        'vqc_zz_2c': VQC(n_layers=2, layer=QuantumLayer.default_variational,
                         state_preparation=QuantumEmbedding.zz,
                         encoder=encoder, epochs=100),
        # 'vqc_zz_angle_2a': VQC(n_layers=2, layer=QuantumLayer.default_variational,
        #                        state_preparation=QuantumEmbedding.zz_angle,
        #                        encoder=encoder),
        # 'vqc_zz_angle_2b': VQC(n_layers=2, layer=QuantumLayer.default_variational,
        #                        state_preparation=QuantumEmbedding.zz_angle,
        #                        encoder=encoder),
        # 'vqc_zz_angle_2c': VQC(n_layers=2, layer=QuantumLayer.default_variational,
        #                        state_preparation=QuantumEmbedding.zz_angle,
        #                        encoder=encoder),
        # 'svc_linear': SKWrapper(model=LinearSVC(), encoder=encoder),
        # 'svc_rbf': SKWrapper(model=SVC(kernel='rbf'), encoder=encoder),
        # 'qke_angle_1': QKE(n_layers=1, kernel=QuantumEmbedding.angle, encoder=encoder),
        # 'qke_angle_2': QKE(n_layers=2, kernel=QuantumEmbedding.angle, encoder=encoder),
        # 'qke_zz_1': QKE(n_layers=1, kernel=QuantumEmbedding.zz, encoder=encoder),
        # 'qke_zz_2': QKE(n_layers=2, kernel=QuantumEmbedding.zz, encoder=encoder),
        # 'qke_zz_angle_1': QKE(n_layers=1, kernel=QuantumEmbedding.zz_angle, encoder=encoder),
        # 'qke_zz_angle_2': QKE(n_layers=2, kernel=QuantumEmbedding.zz_angle, encoder=encoder),
    }
    for clf_name, clf in classifiers.items():
        logging.warning(f'Starting training of {clf_name} on encoding {encoder_name}')
        train_time_start = time.time()
        clf.fit(train_handler)
        train_time_end = time.time()
        logging.warning(f'Train time: {train_time_end - train_time_start}')
        val_time_start = time.time()
        metric = clf.evaluate(val_handler)
        val_time_end = time.time()
        logging.warning(f'Validation time: {val_time_end - val_time_start}')
        logging.warning(f'Validation accuracy {metric.accuracy()}')
        metrics[encoder_name][clf_name] = {
            'metric': metric,
            'train_time': train_time_end - train_time_start,
            'val_time': val_time_end - val_time_start
        }
        pickle.dump(metrics, open(str(Path(['resources', 'test_results', 'intracase_bpic11_vqc.metrics'])), 'wb'))
        del clf_name, clf
