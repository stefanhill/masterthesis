import copy
import datetime
import logging
import pickle
import sys

import numpy as np
from sklearn.model_selection import cross_validate
from sklearn.preprocessing import MinMaxScaler

sys.path.append('/home/david/masterthesis-quantum-ppm/masterthesis/src/')

from ml.encoding.intercase.AverageDelay import AverageDelay
from ml.encoding.intercase.FrequentPreviousActivity import FrequentPreviousActivity
from ml.encoding.intercase.NoPeerCases import NoPeerCases
from ml.encoding.intercase.PeerActivityCount import PeerActivityCount
from ml.encoding.intercase.ResourceCount import ResourceCount
from ml.encoding.intercase.TopBusyResource import TopBusyResource

from data.handler.EventLogHandler import EventLogHandler
from ml.classifier.QKE import QKE
from ml.classifier.quantum.QuantumEmbedding import QuantumEmbedding
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Path import Path

logging.basicConfig(level=logging.DEBUG)
np.random.seed(0)
MEDIAN_CASE_TIME = 20.4
EXPERIMENT_ID = str(datetime.datetime.now().strftime('%Y-%m-%dT%H-%M-%S'))
EXPERIMENT_NAME = 'intercase_rtfm_kernels'
metrics = {}

handler = EventLogHandler()
handler.load(str(Path(['resources', 'test_logs', 'rtfm_small.xes'])))

timedeltas = [datetime.timedelta(weeks=0.15 * MEDIAN_CASE_TIME),
              datetime.timedelta(weeks=0.3 * MEDIAN_CASE_TIME),
              datetime.timedelta(weeks=0.5 * MEDIAN_CASE_TIME)]

skip = ['1850688+peer_cases', '1850688+peer_act', '1850688+res_count', '1850688+avg_delay']

i_ = 0
for timedelta in timedeltas:
    print(f'Inter-case encoding window on {timedelta}')

    handler_ = copy.deepcopy(handler)

    single_encoders = {
        'peer_cases': [NoPeerCases(timeframe=timedelta)],
        'peer_act': [PeerActivityCount(timeframe=timedelta)],
        'res_count': [ResourceCount(timeframe=timedelta)],
        'avg_delay': [AverageDelay(timeframe=timedelta)],
        'freq_act': [FrequentPreviousActivity(timeframe=timedelta, no_activities=2,
                                              normalization=True)],
        'top_res': [TopBusyResource(timeframe=timedelta, no_resources=2, normalization=True)],
    }

    MAX_CLASSES = len(handler.process_definition)

    for encoder_name, encoders in single_encoders.items():

        normalize = encoder_name in ['peer_cases', 'peer_act', 'res_count', 'avg_delay']

        encoder_name = f'{int(timedelta.total_seconds())}+{encoder_name}'

        if encoder_name in skip:
            continue

        builder = EventLogEncodingBuilder() \
            .add(IndexBasedEncoder(window=4, normalization=True))
        for encoder in encoders:
            builder.add(encoder)

        classifiers = {
            'qke_zz_1': QKE(n_layers=1, kernel=QuantumEmbedding.zz, use_jax=True, debug=True),
            'qke_zz_2': QKE(n_layers=2, kernel=QuantumEmbedding.zz, use_jax=True, debug=True),
            'qke_zz_3': QKE(n_layers=3, kernel=QuantumEmbedding.zz, use_jax=True, debug=True),
            'qke_zz_4': QKE(n_layers=4, kernel=QuantumEmbedding.zz, use_jax=True, debug=True),
        }

        metrics[encoder_name] = {}

        print(f'Start to build encoder {encoder_name}')
        X, y = builder.build(handler)

        if normalize:
            X = MinMaxScaler().fit_transform(X)

        for clf_name, clf in classifiers.items():
            print(f'Starting cross validation of {clf_name} on encoding {encoder_name}')
            metrics[encoder_name][clf_name] = cross_validate(clf, X, y, cv=3)
            print(metrics[encoder_name][clf_name])

            pickle.dump(metrics,
                        open(str(Path(['resources', 'test_results', f'{EXPERIMENT_ID}_{EXPERIMENT_NAME}.metrics'])),
                             'wb'))
            del clf_name, clf

        del X, y
    i_ += 1

    del handler_
