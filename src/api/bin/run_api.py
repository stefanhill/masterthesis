import os

import numpy as np
from flask import Flask
from flask import request, jsonify, make_response
from sklearn.base import ClassifierMixin
from sklearn.preprocessing import MinMaxScaler

from api.fun.mappings import APIUtils
from data.DataUtils import DataUtils
from data.handler.EventLogHandler import EventLogHandler
from ml.encoding.BuilderConfiguration import BuilderConfiguration
from ml.encoding.EventLogEncodingBuilder import EventLogEncodingBuilder
from ml.encoding.intracase.IndexBasedEncoder import IndexBasedEncoder
from utils.Utils import Utils

LOGS_FOLDER = 'C:\\git\\masterthesis\\appdata\\temp'
CLASSIFIER_FOLDER = 'C:\\git\\masterthesis\\appdata\\classifier'
ALLOWED_EXTENSIONS = {'txt', 'xes'}

app = Flask(__name__)
app.config['LOGS_FOLDER'] = LOGS_FOLDER
app.config['CLASSIFIER_FOLDER'] = CLASSIFIER_FOLDER


@app.route('/train-classifier', methods=['POST'])
def train_classifier():
    request_data = request.get_json()

    given_name = request_data['given_name']
    classifier_path = os.path.join(app.config['LOGS_FOLDER'], f'{given_name}.classifier')

    train_path = request_data['train_path']
    evaluate_path = request_data['test_path']

    train_handler = EventLogHandler()
    train_handler.load(train_path)

    evaluate_handler = EventLogHandler()
    evaluate_handler.load(evaluate_path)

    evaluate_handler.process_definition = train_handler.process_definition

    intracase_window = 4 if 'Intracase-Steps' not in request_data['parameters'].keys() else int(request_data['parameters']['Intracase-Steps'])
    builder = EventLogEncodingBuilder() \
        .add(IndexBasedEncoder(window=intracase_window))

    median_case_length = DataUtils.determine_median_case_length(train_handler)
    if len(request_data['parameters']['Intercase-Encoders']) > 0:
        for encoder_name in request_data['parameters']['Intercase-Encoders']:
            enc = APIUtils.parse_intercase_encoder(encoder_name,
                                                   median_case_length * float(request_data['parameters']['Intercase-Timedelta']))
            builder.add(enc)

    X_train, y_train = builder.build(train_handler)
    scaler = MinMaxScaler(clip=True)
    scaler.fit(X_train)
    X_train = scaler.transform(X_train)

    classifier: 'ClassifierMixin'
    if f'{given_name}.p' in os.listdir(app.config['CLASSIFIER_FOLDER']):
        classifier = Utils.load_classifier(classifier_path)
    else:
        classifier = APIUtils.get_classifier_by_name(request_data['classifier'], train_handler, request_data)

    classifier.fit(X_train, y_train)

    X_val, y_val = builder.build(evaluate_handler)
    X_val = scaler.transform(X_val)
    metric = classifier.score(X_val, y_val)

    classifier_path = os.path.join(app.config['CLASSIFIER_FOLDER'], f'{given_name}.classifier')
    Utils.save_classifier(classifier, classifier_path)

    APIUtils.store_encoding_pipeline(builder, scaler, app.config['CLASSIFIER_FOLDER'], given_name)

    os.remove(train_path)
    os.remove(evaluate_path)
    return make_response(jsonify({'accuracy': metric}))


@app.route('/predict-classifier', methods=['POST'])
def predict_classifier():
    request_data = request.get_json()
    given_name = request_data['given_name']
    classifier_path = os.path.join(app.config['CLASSIFIER_FOLDER'], f'{given_name}.classifier')
    predict_path = request_data['predict_path']

    data_handler = EventLogHandler()
    data_handler.load(predict_path)

    builder, scaler = APIUtils.load_encoding_pipeline(CLASSIFIER_FOLDER, given_name)

    classifier: 'ClassifierMixin'
    if f'{given_name}.classifier' in os.listdir(CLASSIFIER_FOLDER):
        classifier = Utils.load_classifier(classifier_path)
    else:
        raise NameError('Classifier does not exist!')

    builder_configuration = BuilderConfiguration([0], [data_handler.dataset.get_trace_by_id(0).events[-1].identifier])

    X, _ = builder.build(data_handler, builder_configuration=builder_configuration, predict=True)
    X = scaler.transform(X)

    prediction = classifier.predict_proba(X)

    resp = {}
    for k, v in data_handler.process_definition.activity_to_id.items():
        prob = prediction[0][np.where(classifier.classes_ == v)]
        resp[k] = 0.0 if len(prob) == 0 else float(prob)

    os.remove(predict_path)
    return make_response(jsonify(resp))


if __name__ == '__main__':
    app.run(debug=True)
