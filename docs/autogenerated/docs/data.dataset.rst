data.dataset package
====================

Submodules
----------

data.dataset.EndEvent module
----------------------------

.. automodule:: data.dataset.EndEvent
   :members:
   :undoc-members:
   :show-inheritance:

data.dataset.Event module
-------------------------

.. automodule:: data.dataset.Event
   :members:
   :undoc-members:
   :show-inheritance:

data.dataset.EventLog module
----------------------------

.. automodule:: data.dataset.EventLog
   :members:
   :undoc-members:
   :show-inheritance:

data.dataset.NoneEvent module
-----------------------------

.. automodule:: data.dataset.NoneEvent
   :members:
   :undoc-members:
   :show-inheritance:

data.dataset.StartEvent module
------------------------------

.. automodule:: data.dataset.StartEvent
   :members:
   :undoc-members:
   :show-inheritance:

data.dataset.Trace module
-------------------------

.. automodule:: data.dataset.Trace
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: data.dataset
   :members:
   :undoc-members:
   :show-inheritance:
