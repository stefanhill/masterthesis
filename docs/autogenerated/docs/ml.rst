ml package
==========

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   ml.classifier
   ml.encoding
   ml.metrics

Submodules
----------

ml.AbstractClassifier module
----------------------------

.. automodule:: ml.AbstractClassifier
   :members:
   :undoc-members:
   :show-inheritance:

ml.AbstractEncoder module
-------------------------

.. automodule:: ml.AbstractEncoder
   :members:
   :undoc-members:
   :show-inheritance:

ml.AbstractMetric module
------------------------

.. automodule:: ml.AbstractMetric
   :members:
   :undoc-members:
   :show-inheritance:

ml.AbstractPrediction module
----------------------------

.. automodule:: ml.AbstractPrediction
   :members:
   :undoc-members:
   :show-inheritance:

ml.EventLogEncoder module
-------------------------

.. automodule:: ml.EventLogEncoder
   :members:
   :undoc-members:
   :show-inheritance:

ml.MLUtils module
-----------------

.. automodule:: ml.MLUtils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: ml
   :members:
   :undoc-members:
   :show-inheritance:
